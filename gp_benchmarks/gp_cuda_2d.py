import numpy as np
import torch
import argparse
import matplotlib
import torch.multiprocessing as mp 
# mp.set_start_method('spawn')
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from torch.autograd import Variable
from torch import distributions as td
import os

# Logging
from logger import logger, setup_logger
from logger import create_stats_ordered_dict

parser = argparse.ArgumentParser()
parser.add_argument("--num_fakes", type=int, default=20, help="number of epochs of fake examples drawn in the relabelling process")
parser.add_argument("--num_dim", type=int, default=6, help="Number of dimensions of inputs in the process")
parser.add_argument("--use_expl", action='store_true', help="Whether to use or not use exploration")
parser.add_argument("--anneal_expl", action='store_true', help="Whether to anneal exploration or not")
parser.add_argument("--disc_only", action='store_true', help="Discriminator only training of ")
parser.add_argument("--add_fakes", action='store_true', help='Add fakes or not')
parser.add_argument("--weighting_scheme", type=str, default="none", help="Adding weighting scheme for the model")
parser.add_argument("--expl_weighting_scheme", type=str, default="none", help="Exploration weighting scheme")
parser.add_argument("--lamda", type=float, default=1.0, help="default for lamda -- the fully connected reconstruction coefficient")
parser.add_argument("--nu", type=float, default=0.5, help="default for nu -- the fully connected prior coefficient")
parser.add_argument("--log_dir", type=str, default="./data/rastrigin10d_new/", help="Data directory for storing logs")
parser.add_argument("--function_limit", type=float, default=0.0, help="function limit on the lower side")
parser.add_argument("--ngpu", type=int, default=0, help="Gpu ids")
parser.add_argument("--noise_dim", type=int, default=20, help="Noise dimension")
opt = parser.parse_args()

os.environ["CUDA_VISIBLE_DEVICES"] = str(opt.ngpu)

def softmax(arr, temp=1.0):
  max_arr = arr.max()
  arr_new = arr - max_arr
  exp_arr = np.exp(arr_new/temp)
  return exp_arr / np.sum(exp_arr)

def softmin(arr, temp=1.0):
  return softmax(-arr, temp=temp)

def weighted_softmax(arr, weights, temp=1.0):
  max_arr = arr.max()
  arr_new = arr - max_arr
  exp_arr = np.exp(arr_new/temp)
  wt_exp_arr = exp_arr * weights
  return wt_exp_arr/np.sum(wt_exp_arr)

def weighted_softmin(arr, weights, temp=1.0):
  return weighted_softmax(-arr, weights, temp=1.0)

# Problem definition
bounds = [[-5.0, 10.0], [0.0, 15.0]]

def f(x, y):
  X = np.concatenate([np.expand_dims(x, -1), np.expand_dims(y, -1)], -1)
  a = 1
  b = 5.1/ ( 4 * np.pi * np.pi)
  c = 5 / np.pi
  r = 6
  s = 10
  t = 1.0/ (8 * np.pi)
  return a * ((X[:, :, 1] - b * (X[:, :, 0]**2) + c * X[:, :, 0] - r)**2) + s * (1 - t) * np.cos(X[:, :, 0]) + s

def f_point(x, y):
  a = 1
  b = 5.1/ ( 4 * np.pi * np.pi)
  c = 5 / np.pi
  r = 6
  s = 10
  t = 1.0/ (8 * np.pi)
  return a * ((y - b * (x**2) + c * x - r)**2) + s * (1 - t) * np.cos(x) + s 

def f_torch(x):
  a = 1
  b = 5.1/ ( 4 * np.pi * np.pi)
  c = 5 / np.pi
  r = 6
  s = 10
  t = 1.0/ (8 * np.pi)
  return a * ((x[:, 1] - b * (x[:, 0]**2) + c * x[:, 0] - r)**2) + s * (1 - t) * torch.cos(x[:, 0]) + s 

def plot_func():
  X = np.arange(bounds[0][0], bounds[0][1], 0.25)
  Y = np.arange(bounds[1][0], bounds[1][1], 0.25)
  X, Y = np.meshgrid(X, Y)
  fig = plt.figure()
  ax = fig.gca(projection='3d')
  Z = f(X, Y)

  surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                         linewidth=0, antialiased=False)
 

def plot_func_with_pt(pt):
  X = np.arange(bounds[0][0], bounds[0][1], 0.25)
  Y = np.arange(bounds[1][0], bounds[1][1], 0.25)
  X, Y = np.meshgrid(X, Y)
  fig = plt.figure()
  ax = fig.gca(projection='3d')
  Z = f(X, Y)

  surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                         linewidth=0, antialiased=False)
  print (ax)
  ax.set_xlabel('x1')
  ax.set_ylabel('x2')
  ax.set_zlabel('f(x1, x2)')
  ax.scatter(pt[0], pt[1], f_point(pt[0], pt[1]), s=50, c='r')
 

class ForwardModel(torch.nn.Module):
  def __init__(self, layers, inp_dim, output_dim,):
    super(ForwardModel, self).__init__()
    self.layers = layers
    self.inp_dim = inp_dim
    self.output_dim = output_dim

    network = []
    network.append(torch.nn.Linear(self.inp_dim, self.layers[0]))
    network.append(torch.nn.ReLU())
    for idx, layer_size in enumerate(layers[1:]):
      network.append(torch.nn.Linear(self.layers[idx], layer_size))
      network.append(torch.nn.ELU())
    
    network.append(torch.nn.Linear(self.layers[-1], self.output_dim))
    self.network = torch.nn.Sequential(*network)
  
  def forward(self, x):
    return self.network(x)
  
  def train(self, dataset):
    y = dataset['score'].type(torch.cuda.FloatTensor)
    x = dataset['location'].type(torch.cuda.FloatTensor)
    out_x = self.forward(x)
    loss = torch.nn.MSELoss()(out_x, y)
    return loss

  def reset_weights(self):
    for layer in self.network:
      if hasattr(layer, 'reset_parameters'):
        layer.reset_parameters()

def find_best_y(net, forward_net, y_start, num_itrs=100, lamda=0.2, nu=0.01, noise_dim=3):
    """Find the best possible y for a given x"""
    # import ipdb; ipdb.set_trace()
    y = Variable(y_start, requires_grad=True)
    z = torch.rand(noise_dim, requires_grad=True, device='cuda')
    opt_y_z = torch.optim.Adam([y, z], lr=0.001)
    std_normal = td.Normal(loc=0.0, scale=1.0)
    net.eval()

    for idx in range(num_itrs):
        with torch.set_grad_enabled(True):
            sampled_x = net.forward_tmp(y.unsqueeze(0), noise=z.unsqueeze(0))

            ## This example has minimization of score, hence, the difference in the sign of the Approx-Infer procedure below, as compared to gp_cuda_1d.py
            loss_t = forward_net(sampled_x).sum() + lamda * (forward_net(sampled_x) - y.type(torch.cuda.FloatTensor).unsqueeze(0).detach()).pow(2).sum() -\
                        nu * std_normal.log_prob(z).sum()
        opt_y_z.zero_grad()
        loss_t.backward()
        opt_y_z.step()

    final_out = net.forward_tmp(y.unsqueeze(0), noise=z.unsqueeze(0)).detach().cpu().numpy()
    net.train()
    return final_out    

class ModelInversionNetwithDiscriminator(torch.nn.Module):
  """Model Inversion Network with a discriminator"""
  def __init__(self, layers, inp_dim, noise_dim, output_dim, use_bounds=True, lipschitz_constant=0.1, lamda=0.01):
    super(ModelInversionNetwithDiscriminator, self).__init__()
    self.layers = layers
    self.inp_dim = inp_dim
    self.noise_dim = noise_dim
    self.output_dim = output_dim
    self.lipschitz_constant = lipschitz_constant
    self.lamda = lamda
    gen_network = []
    
    gen_network.append(torch.nn.Linear(self.inp_dim + self.noise_dim, self.layers[0]))
    gen_network.append(torch.nn.LeakyReLU(0.2, inplace=True))
    for idx, layer_size in enumerate(layers[1:]):
      gen_network.append(torch.nn.Linear(self.layers[idx], layer_size))
      gen_network.append(torch.nn.BatchNorm1d(layer_size, 0.8))
      gen_network.append(torch.nn.LeakyReLU(0.2, inplace=True))
    gen_network.append(torch.nn.Linear(self.layers[-1], self.output_dim))
    
    self.use_bounds = use_bounds
    if self.use_bounds:
      self.bounds = np.array(bounds)
      self.tanh_size = torch.Tensor((self.bounds[:, 1] - self.bounds[:, 0])* 0.5).to(device)
      self.mean_bounds = torch.Tensor((self.bounds[:, 1] + self.bounds[:, 0])*0.5).to(device)
      print ('tanh_size: ', self.tanh_size)
      print ('mean_bounds: ', self.mean_bounds)
      print ('bounds: ', self.bounds)
      gen_network.append(torch.nn.Tanh())
    
    self.gen_network = torch.nn.Sequential(*gen_network)
    
    discriminator_net = []
    discriminator_net.append(torch.nn.Linear(self.output_dim + self.inp_dim, self.layers[0]))
    discriminator_net.append(torch.nn.LeakyReLU(0.2, inplace=True))

    for idx, layer_size in enumerate(layers[1:]):
      discriminator_net.append(torch.nn.Linear(self.layers[idx], layer_size))
      discriminator_net.append(torch.nn.Dropout(0.4))
      discriminator_net.append(torch.nn.LeakyReLU(0.2, inplace=True))
    
    discriminator_net.append(torch.nn.Linear(self.layers[-1], 1))
    self.discriminator = torch.nn.Sequential(*discriminator_net)

  def forward(self, score):
    random_noise = td.Normal(loc=0.0, scale=1.0).sample(sample_shape=(score.shape[0], self.noise_dim))
    input_in = torch.cat([score.type(torch.cuda.FloatTensor), random_noise.type(torch.cuda.FloatTensor)], -1) #.to(device)
    output = self.gen_network(input_in)
    if self.use_bounds:
      output = self.tanh_size.unsqueeze(0) * output + self.mean_bounds.unsqueeze(0)
    return output
  
  def forward_tmp(self, score, noise):
    input_in = torch.cat([score.type(torch.cuda.FloatTensor), noise.type(torch.cuda.FloatTensor)], -1)
    output = self.gen_network(input_in)
    if self.use_bounds:
      output = self.tanh_size.unsqueeze(0) * output + self.mean_bounds.unsqueeze(0)
    return output
  
  def reset_weights(self):
    for layer in self.gen_network:
      if hasattr(layer, 'reset_parameters'):
        layer.reset_parameters()
    
    for layer in self.discriminator:
      if hasattr(layer, 'reset_parameters'):
        layer.reset_parameters()

  def set_weights_to_other(self, other_net):
    for w, target in zip(self.gen_network.parameters(), other_net.gen_network.parameters()):
      w.data = target.data

    for w, target in zip(self.discriminator.parameters(), other_net.discriminator.parameters()):
      w.data = target.data
  
  def discriminator_forward(self, score, point):
    input_in = torch.cat([score.type(torch.cuda.FloatTensor), point.type(torch.cuda.FloatTensor)], -1)
    validity = self.discriminator(input_in.to(device))
    return validity

def train_net_wd(net, optimizer_g, optimizer_d, batch, add_fakes=False, disc_only=False):
  output = net(batch['score'].to(device))
  validity = net.discriminator_forward(batch['score'].to(device), output)
  gen_loss = torch.nn.MSELoss()(validity, torch.ones_like(validity).type(torch.cuda.FloatTensor)) #.to(device))
  #  0.0*net.lipschitz_gen(batch['score'].to(device))
  if add_fakes and not disc_only:
      scores_t = get_random_scores(batch['score'])
      output_t = net(scores_t)
      validity_t = net.discriminator_forward(scores_t, output_t)
      gen_loss_fake = torch.nn.MSELoss()(validity_t, torch.ones_like(validity_t).type(torch.cuda.FloatTensor))
      gen_loss = (gen_loss + gen_loss_fake)*0.5

  net.zero_grad()
  gen_loss.backward()
  optimizer_g.step()
  
  validity_real = net.discriminator_forward(batch['score'].to(device), batch['location'].type(torch.cuda.FloatTensor))  #.to(device))
  d_real_loss = torch.nn.MSELoss()(validity_real, torch.ones_like(validity_real).type(torch.cuda.FloatTensor))  #.to(device))
  validity_fake = net.discriminator_forward(batch['score'].to(device), output.detach())
  d_fake_loss = torch.nn.MSELoss()(validity_fake, torch.zeros_like(validity_fake).type(torch.cuda.FloatTensor))  #.to(device))
  d_loss = (d_real_loss + d_fake_loss)/2

  if add_fakes:
      scores_t = get_random_scores(batch['score'])
      output_t = net(scores_t)
      validity_t = net.discriminator_forward(scores_t, output_t.detach())
      d_fake_t = torch.nn.MSELoss()(validity_t, torch.zeros_like(validity_t).type(torch.cuda.FloatTensor))
      d_loss = (d_real_loss + (d_fake_loss + d_fake_t)*0.5)*0.5

  net.zero_grad()
  d_loss.backward()
  optimizer_d.step()
  
  return gen_loss, d_loss

def weighted_mse_loss(input, target, weight):
    return torch.sum(weight * (input - target) ** 2)

def get_random_scores(score_init):
    batch_size = score_init.shape[0]
    return torch.clamp(score_init.type(torch.cuda.FloatTensor), min=0.0) + torch.randn(*score_init.shape).type(torch.cuda.FloatTensor)*0.4

def train_net_wd_with_weighting(net, optimizer_g, optimizer_d, batch, weights, add_fakes=False, disc_only=False, weight_fn=None, temp_given=None):
  """Train model inversion net with discriminator via weighting"""
  output = net(batch['score'].to(device))
  validity = net.discriminator_forward(batch['score'].to(device), output)
  gen_loss = weighted_mse_loss(validity, torch.ones_like(validity).type(torch.cuda.FloatTensor), weights.to(device).unsqueeze(-1))
  if add_fakes and not disc_only:
      scores_t = get_random_scores(batch['score'])
      output_t = net(scores_t)
      validity_t = net.discriminator_forward(scores_t, output_t)
      weights_t = weight_fn(batch, temp_given=temp_given)
      gen_loss_t = weighted_mse_loss(validity_t, torch.ones_like(validity_t).type(torch.cuda.FloatTensor), weights_t.to(device))
      gen_loss = (gen_loss + gen_loss_t)*0.5

  net.zero_grad()
  gen_loss.backward()
  optimizer_g.step()
  
  validity_real = net.discriminator_forward(batch['score'].to(device), batch['location'].type(torch.cuda.FloatTensor)) #.to(device))
  d_real_loss = weighted_mse_loss(validity_real, torch.ones_like(validity_real).type(torch.cuda.FloatTensor), weights.unsqueeze(-1))
  validity_fake = net.discriminator_forward(batch['score'], output.detach())
  d_fake_loss = weighted_mse_loss(validity_fake, torch.zeros_like(validity_fake).type(torch.cuda.FloatTensor), weights.unsqueeze(-1))
  d_loss = (d_real_loss + d_fake_loss)/2

  if add_fakes:
      scores_t = get_random_scores(batch['score'])
      output_t = net(scores_t)
      validity_t = net.discriminator_forward(scores_t, output_t.detach())
      weights_t = weight_fn(batch, temp_given=temp_given)
      d_fake_temp = weighted_mse_loss(validity_t, torch.zeros_like(validity_t).type(torch.cuda.FloatTensor), weights_t.unsqueeze(-1))
      d_loss = d_real_loss + (d_fake_loss + d_fake_temp)*0.5
  
  net.zero_grad()
  d_loss.backward()
  optimizer_d.step()
  
  return gen_loss, d_loss

def train_epochs_wd(net, optimizer_g, optimizer_d, dataset, num_epochs=10):
  all_losses = []
  for idx in range(num_epochs):
    loss_val = train_net_wd(net, optimizer_g, optimizer_d, dataset, add_fakes=opt.add_fakes, disc_only=opt.disc_only)
    all_losses.append(loss_val[0].cpu().detach().numpy())
  print ('Fitting Loss: ', np.array(all_losses).mean())
  return all_losses

def adaptive_temp(scores_np, base_temp=1.0):
    if len(scores_np) <= 5:
        return base_temp
    else:
        max_arr = scores_np.min()
        scores_new = scores_np - max_arr
        quantile_ninety = np.quantile(scores_new, q=0.1)
        return np.abs(quantile_ninety)

def adaptive_temp_v2(scores_np, base_temp=1.0):
    if len(scores_np) <= 5:
        return base_temp
    else:
        # The difference in sign from the gp_cuda_1d.py file is bbecause here, we are minimizing the score. 
        inverse_arr = -scores_np
        max_score = inverse_arr.max()
        scores_new = inverse_arr - max_score
        quantile_ninety = np.quantile(scores_new, q=0.9)
        return np.abs(quantile_ninety)

def train_epochs_wd_with_weighting(net, optimizer_g, optimizer_d, dataset, num_epochs=10, weighting_scheme='uniform'):
  all_losses = []
  batch = dataset
  if batch['score'].shape[0] <= 5 or weighting_scheme == 'none':
    weights = torch.ones_like(batch['score']).type(torch.FloatTensor)[:, 0] # 1

  elif weighting_scheme == 'biased_softmin':
    # Uniform weighting shceme over all elements in the batch by score
    # import ipdb; ipdb.set_trace()
    scores_np = batch['score'][:, 0].cpu().numpy()
    hist, bin_edges = np.histogram(scores_np, bins=20)
    hist = hist/np.sum(hist)
    base_temp = 0.1
    base_temp = adaptive_temp_v2(scores_np, base_temp)
    softmin_prob = softmin(bin_edges[1:], temp=base_temp)

    bin_indices = np.digitize(scores_np, bin_edges[1:])
    hist_prob = hist[np.minimum(bin_indices, 19)]
    
    provable_dist = softmin_prob * (hist / (hist + 1e-3))
    provable_dist = provable_dist / (np.sum(provable_dist) + 1e-7)
    provable_dist_prob = provable_dist[np.minimum(bin_indices, 19)]

    weights = provable_dist_prob / (hist_prob + 1e-7)
    weights = torch.Tensor(weights)
    weights = torch.clamp(weights, min=0.0, max=5.0)

  elif weighting_scheme == 'uniform':
    scores_np = batch['score'][:, 0].cpu().numpy()
    hist, bin_edges = np.histogram(scores_np, bins=10)
    hist = hist / np.sum(hist)
    skewed_hist = 1/(hist + 1e-6) * hist / (hist + 1e-6)
    skewed_hist = skewed_hist / np.sum(skewed_hist)
    bin_indices = np.digitize(scores_np, bin_edges[1:])
    weights = skewed_hist[np.minimum(bin_indices, 9)]
    #     print (weights)
  
#   print (weights)
  weights = torch.Tensor(weights).to(device)
  all_losses = []
  for idx in range(num_epochs):
    loss_val = train_net_wd_with_weighting(net, optimizer_g, optimizer_d, dataset, weights, add_fakes=opt.add_fakes, disc_only=opt.disc_only, weight_fn=get_weights, temp_given=None)
    if idx % 1000 == 0:
        print ('Loss val: ', weighting_scheme, loss_val[0].item(), loss_val[1].item())
    all_losses.append(loss_val[0].cpu().detach().numpy())
  print ('Fitting Loss Weighting: ', weighting_scheme, np.array(all_losses).mean())
  return all_losses

def get_weights(dataset, weighting_scheme='biased_softmin', temp_given=None):
    """Get the weights used for reweighting"""
    batch = dataset
    if weighting_scheme == 'none':
        weights = torch.ones_like(batch['score']).type(torch.FloatTensor)[:, 0] # 1
    elif weighting_scheme == 'biased_softmin':
        # Uniform weighting shceme over all elements in the batch by score
        # import ipdb; ipdb.set_trace()
        scores_np = batch['score'][:, 0].cpu().numpy()
        hist, bin_edges = np.histogram(scores_np, bins=20)
        hist = hist/np.sum(hist)
        base_temp = 1.0
        if temp_given is None:
          base_temp = adaptive_temp_v2(scores_np, base_temp)
        else:
          base_temp = temp_given
        softmin_prob = softmin(bin_edges[1:], temp=base_temp)

        provable_dist = softmin_prob * (hist/ (hist + 1e-3))
        provable_dist = provable_dist / (np.sum(provable_dist) + 1e-7)

        bin_indices = np.digitize(scores_np, bin_edges[1:])
        hist_prob = hist[np.minimum(bin_indices, 19)]

        weights = provable_dist[np.minimum(bin_indices, 19)]  / (hist_prob + 1e-7)
        weights = torch.Tensor(weights)
        weights = torch.clamp(weights, min=0.0, max=5.0)

    elif weighting_scheme == 'uniform':
        scores_np = batch['score'][:, 0].cpu().numpy()
        hist, bin_edges = np.histogram(scores_np, bins=20)
        hist = hist / np.sum(hist)
        skewed_hist = 1/(hist + 1e-6) * hist / (hist + 1e-6) * 0.05
        bin_indices = np.digitize(scores_np, bin_edges[1:])
        weights = skewed_hist[np.minimum(bin_indices, 19)]
    
    weights = torch.Tensor(weights).to(device)
    weights = torch.clamp(weights, min=0.0, max=5.0)
    return weights

def exploration_data(dataset, scheme='thompson_sampling'):
    current_scores = dataset['score'][:, 0]
    current_scores_np = current_scores.detach().cpu().numpy()
    data_points_np = dataset['location'].detach().cpu().numpy()

    if scheme == 'thompson_sampling':
        """We do this by creating fakes and training on the fake data"""
        fake_data_np = np.random.uniform(low=np.array(bounds)[:, 0], high=np.array(bounds)[:, 1], size=(opt.num_fakes, opt.num_dim))
        base_temp = 2.0
        base_temp = adaptive_temp_v2(current_scores_np, base_temp)
        print ('Base temp: ', base_temp)
        scores = softmin(current_scores_np, temp=base_temp)
        quantile_score = np.quantile(current_scores_np, q=0.1)

        fake_data_scores = np.random.uniform(opt.function_limit, min(quantile_score, 10.0), size=opt.num_fakes)

        scores_torch = torch.Tensor(fake_data_scores).to(device)
        fake_data_torch = torch.Tensor(fake_data_np).to(device)

        dataset['score'] = torch.cat([current_scores, scores_torch.type(torch.cuda.DoubleTensor)], 0).unsqueeze(-1)
        dataset['location'] = torch.cat([dataset['location'], fake_data_torch.type(torch.cuda.DoubleTensor)], 0)
    return dataset

def get_query_point(dataset, net, scheme='softmax_extrapolated'):
  current_scores = dataset['score'][:, 0]
  current_scores_np = current_scores.detach().cpu().numpy()
  if scheme == 'softmax_extrapolated':
    softmin_score = softmin(current_scores_np, temp=0.1)
#     print ('Query score: ', softmin_score)
    sample_softmin = np.random.choice(current_scores_np, p=softmin_score)
    random_noise = np.abs((np.random.normal(loc=0.0, scale=0.1)))
    score_query = np.maximum(sample_softmin - random_noise, opt.function_limit)
  elif scheme == 'softmax':
    softmin_score = softmin(current_scores_np, temp=2.0)
    sample_softmin = np.random.choice(current_scores_np, p=softmin_score)
    score_query = sample_softmin
  
  score_query = torch.tensor(score_query)
  score_query = score_query.unsqueeze(0).unsqueeze(0)    # 1 x 1
  with torch.no_grad():
    net.eval()
    out_point = net(score_query)
    net.train()
  return out_point, score_query

def get_errors(dataset, net):
  current_scores = dataset['score']
  out_points = net(current_scores)

# Bayesian Optimization scheme
n_iter = 1000

device = 'cpu'
if torch.cuda.is_available():
  device = 'cuda'

def train_forward_model(forward_model, forward_opt, dataset, iteration, weighting_scheme='uniform'):
    """Train the forward model"""
    weights = get_weights(dataset, weighting_scheme=weighting_scheme)
    for j in range(10000):
            forward_loss = forward_model.train(dataset)
            forward_model.zero_grad()
            forward_loss.backward()
            forward_opt.step()
    print ('Iteration: %d, Forward Loss: %f' % (iteration, forward_loss.item()))

if __name__ == '__main__':
    mp.set_start_method('spawn')
    x_init = torch.tensor(np.array([[0.0, 0.0], [-1.0, 0.3]])).to(device)
    f_x_init = f_torch(x_init)
    dataset = dict()
    dataset['score'] = f_x_init.unsqueeze(-1).type(torch.cuda.DoubleTensor)
    dataset['location'] = x_init.type(torch.cuda.DoubleTensor)
    reset_parameters = False
    seed = np.random.randint(100, 10000)

    variant = vars(opt)
    dir_name = "-".join([str(variant[x]) for x in variant.keys()]) + '-reset_false'
    print ('Logging to... ', dir_name)
    setup_logger(dir_name + '-' + str(seed), variant=variant, log_dir=(opt.log_dir + dir_name + '-' + str(seed)))

    print (os.path.join(opt.log_dir, dir_name, str(seed)))
    cont_net = ModelInversionNetwithDiscriminator(layers=(512, 512), inp_dim=1, noise_dim=opt.noise_dim,
                            output_dim=2, use_bounds=True)
    if opt.use_expl:
        expl_net = ModelInversionNetwithDiscriminator(layers=(512, 512), inp_dim=1, noise_dim=opt.noise_dim,
                                                    output_dim=2, use_bounds=True)
    forward_model = ForwardModel(layers=(512, 512), inp_dim=2, output_dim=1)
    cont_net.reset_weights()
    if opt.use_expl:
        expl_net.reset_weights()
    forward_model.reset_weights()

    if torch.cuda.is_available():
        cont_net.cuda()
        cont_net.gen_network.cuda()
        cont_net.discriminator.cuda()
        if opt.use_expl:
            expl_net.cuda()
            expl_net.gen_network.cuda()
            expl_net.discriminator.cuda()
        forward_model.cuda()

    # Use share memory for multi-processing
    cont_net.share_memory()
    if opt.use_expl:
        expl_net.share_memory()
    forward_model.share_memory()
    
    cont_net_gen_opt = torch.optim.Adam(cont_net.gen_network.parameters(), lr=0.0002, betas=(0.5, 0.999))
    cont_net_disc_opt = torch.optim.Adam(cont_net.discriminator.parameters(), lr=0.0002, betas=(0.5, 0.999))

    if opt.use_expl:
        expl_net_gen_opt = torch.optim.Adam(expl_net.gen_network.parameters(), lr=0.0002, betas=(0.5, 0.999))
        expl_net_disc_opt = torch.optim.Adam(expl_net.discriminator.parameters(), lr=0.0002, betas=(0.5, 0.999))

    forward_opt = torch.optim.Adam(forward_model.parameters(), lr=0.001)

    print ('Init Point: ', x_init, f_x_init)
    function_min = []

    for i in range(n_iter):
        # First train the model on the current points
        p = mp.Process(target=train_epochs_wd_with_weighting, args=(cont_net, cont_net_gen_opt, cont_net_disc_opt, dataset, 3000, opt.weighting_scheme))
        p.start()

        # Training exploration module
        if opt.anneal_expl and i > 15 and i in [64, 128, 256]:
            opt.num_fakes = opt.num_fakes // 2
        
        fake_data = exploration_data(dataset)
        
        # Train the exploration net model
        if opt.use_expl:
            p_expl = mp.Process(target=train_epochs_wd_with_weighting, args=(expl_net, expl_net_gen_opt, expl_net_disc_opt, fake_data, 3000, opt.expl_weighting_scheme))
            p_expl.start()
        
        p_fwd = mp.Process(target=train_forward_model, args=(forward_model, forward_opt, dataset, i, opt.weighting_scheme))
        p_fwd.start()

        p.join()
        if opt.use_expl:
            p_expl.join()
        p_fwd.join()

        if i > 0:
            x_ret = find_best_y(cont_net, forward_model, y_start=dataset['score'][-1], num_itrs=1000, lamda=opt.lamda, nu=opt.nu, noise_dim=opt.noise_dim)
            print ('Returned point: ', x_ret, f_point(x_ret[0][0], x_ret[0][1]))
            logger.record_tabular('Extrapolated score', f_point(x_ret[0][0], x_ret[0][1]))
        else:
            logger.record_tabular('Extrapolated score', 10.0)
        
        # Now develop a new way of querying for the new datapoint, so as to
        # account for increasing the score
        # With TS, we use the exploitation net for finding out which one has the highest score
        if opt.use_expl:
            query_net = expl_net
        else:
            query_net = cont_net
        
        query_net.eval()
        new_query_point, score_query = get_query_point(dataset, query_net, scheme='softmax_extrapolated')
        new_query_point = new_query_point.detach()
        query_net.train()

        print ('New expl query point: ', new_query_point[0].cpu().numpy(), f_point(new_query_point[0].cpu().numpy()[0], new_query_point[0].cpu().numpy()[1]), score_query[0][0].cpu().detach().numpy())
        
        logger.record_tabular('Exploration Queried', score_query[0][0].cpu().detach().numpy())
        logger.record_tabular('Exploration Observed', f_point(new_query_point[0].cpu().numpy()[0], new_query_point[0].cpu().numpy()[1]))

        # Add new point to dataset
        dataset['location'] = torch.cat([dataset['location'], new_query_point.type(torch.DoubleTensor).to(device)], 0)
        dataset['score'] = f_torch(dataset['location']).unsqueeze(-1)

        # Also test what the best of the cont_net is:
        cont_net.eval()
        new_query_point, score_query = get_query_point(dataset, cont_net, scheme='softmax_extrapolated')
        new_query_point = new_query_point.detach()
        cont_net.train()
        print ('Eval point: ', new_query_point[0].cpu().numpy(), f_point(new_query_point[0].cpu().numpy()[0], new_query_point[0].cpu().numpy()[1]), score_query[0][0].cpu().detach().numpy())
        
        logger.record_tabular('Original CN Queried', score_query[0][0].cpu().detach().numpy())
        logger.record_tabular('Original CN Observed', f_point(new_query_point[0].cpu().numpy()[0], new_query_point[0].cpu().numpy()[1]))
        
        function_min.append(f_point(new_query_point[0].cpu().numpy()[0], new_query_point[0].cpu().numpy()[1]))
        
        # Optinally reset parameters after every training run
        if reset_parameters:
            cont_net.reset_weights()
            if opt.use_expl:
                expl_net.reset_weights()
            forward_model.reset_weights()
        
        logger.dump_tabular()
