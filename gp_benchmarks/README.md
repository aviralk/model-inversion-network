# Model Inversion Networks: Code for GP benchmark experiments
In this directory, we provide code for the GP benchmark experiments in Model Inversion networks (Section 4.2: Global optimization of benchmark functions)

## Requirements
We use basic requirements as [rlkit](https://github.com/vitchyr/rlkit), and the easiest way to install all requirements for running our code for this experiment is to install rlkit environment. We operate in a linux-gpu environment, with PyTorch-1.1 and Cuda 10. We would recommend installing the `linux-gpu` conda-env from rlkit for this experiment.

Also add the rlkit directory to PYTHONPATH:
```
export PYTHONPATH=/path/to/rlkit:$PYTHONPATH
```

## Running an experiment
A sample command for running an experiment on a simple 1D domain (runs very fast, within 20-30 iterations) is:
```
python gp_cuda_1d.py --num_fakes=10 --num_dim=1 --use_expl --anneal_expl --weighting_scheme='biased_softmax' --log_dir=<path-to-log-dir> --expl_weighting_scheme='none' --ngpu=<gpu id>
```

Similarly for the 2D Branin benchmark (takes 400-600 iterations), use the following to run an experiment:
```
python gp_cuda_2d.py --num_fakes=10 --num_dim=2 --use_expl --anneal_expl --weighting_scheme='biased_softmin' --log_dir=<path-to-log-dir> --expl_weighting_scheme='biased_soft min' --ngpu=<gpu id>
```

## Command-line Arguments/Hyperparameters:
The set of command line arguments and hyperparameters is explained below:
1. `num_fakes`: Number of synthetic data points to add for training the exploration model via randomized-labeling (Section 3.4)
2. `num_dim`: Dimensionality of the input being reconstructed
3. `use_expl`: Whether to do exploration/active data collection via randomized labeling, or to just use simple greedy / epsilon-greedy over the current model
4. `weighting_scheme`: The reweighting (Section 3.3) formulat to be used (There are two options: `biased_softmin` and `biased_softmax` -- they implemment the same formula, with the exeception of the sign of the score, so use `biased_softmin` when minimizing a score, and `biased_softmax` when maximizing a score.)
5. `expl_weighting_scheme`: which weighting scheme to use for the exploration model, we just set it to `none` as default, however, setting it to the same scheme as `weighting_scheme` works well too.
6. `ngpu`: the gpu-id to run this code on

For other commandline arguments, we just keep them fixed to the default values as mentioned in the `gp_cuda_1d.py` file.