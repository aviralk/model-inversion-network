This is the code for Model Inversion Networks for Model-Based Optimization.

## Model Inversion Networks for Model-Based Optimization
   Aviral Kumar, Sergey Levine.
   https://arxiv.org/abs/1912.13464
   
For running each of the experiments, please refer to the code inside each directory. There is code for three experiments -- 
(1) Classification cast as bandit learning problem (Table 1)\\
(2) Face Optimization with MINs (Figure 3)\\
(3) Common GP benchmarks (Table 2)\\

For running each of these experiments, please find detailed instructions in readmes inside directories corresponding to each experiment.

If this codebase is useful in your research, please cite our paper:
```
@article{kumar19min,
  author       = {Aviral Kumar and Sergey Levine},
  title        = {Model Inversion Networks for Model-Based Optimization},
  url          = {https://arxiv.org/abs/1912.13464},
}
```

For any questions or concerns, please contact: Aviral Kumar at aviralk@berkeley.edu.