import torch
from torch.utils.data import Dataset
from torchvision.datasets import MNIST
from torchvision.datasets import CIFAR10
import numpy as np 

def softmax(arr, temp=1.0):
  max_arr = arr.max()
  arr_new = arr - max_arr
  exp_arr = np.exp(arr_new/temp)
  return exp_arr / np.sum(exp_arr)

def one_hot(nparray, depth=0, on_value=1, off_value=0):
    if depth == 0:
        depth = np.max(nparray) + 1
    assert np.max(nparray) < depth, "the max index of nparray: {} is larger than depth: {}".format(np.max(nparray), depth)
    shape = nparray.shape
    out = np.ones((*shape, depth)) * off_value
    indices = []
    for i in range(nparray.ndim):
        tiles = [1] * nparray.ndim
        s = [1] * nparray.ndim
        s[i] = -1
        r = np.arange(shape[i]).reshape(s)
        if i > 0:
            tiles[i-1] = shape[i-1]
            r = np.tile(r, tiles)
        indices.append(r)
    indices.append(nparray)
    out[tuple(indices)] = on_value
    return out

class BanditCIFAR(CIFAR10):
    def __init__(self, root, train=True, transform=None, target_transform=None, download=False, random_policy_type='uniform', weighting_type='uniform'):
        super(BanditCIFAR, self).__init__(root=root, train=train, transform=transform, target_transform=target_transform, download=download)
        self.random_policy_type = random_policy_type
        self.num_targets = 10

        self.rewards = []
        self.int_targets = []
        self.chosen_targets = []
        self.log_prob = []
        for idx, (img, target) in enumerate(zip(self.data, self.targets)):
            target = int(self.targets[idx])
            self.int_targets.append(target)
            if self.random_policy_type == 'biased':
                action = np.random.choice(10)
                log_prob = np.log(0.1)
            elif self.random_policy_type == 'uniform':
                prob_vec = 5.0/(self.num_targets - 1) * np.ones((self.num_targets,))
                prob_vec[target] = 5.0
                prob_vec = 1.0/self.num_targets * prob_vec
                action = np.random.choice(self.num_targets, p=prob_vec)
                log_prob = np.log(prob_vec[action])
            elif self.random_policy_type == 'oracle':
                action = target
                log_prob = 0.0
            else:
                raise NotImplementedError
            
            reward = np.clip(2.0 - 1.5*np.abs(action - target), 0.5, 2.0)
            self.rewards.append(reward)
            self.chosen_targets.append(one_hot(np.array([action,]), depth=self.num_targets,)[0])
            self.log_prob.append(log_prob)
        
        # import ipdb; ipdb.set_trace()
        self.int_targets = np.array(self.int_targets)
        self.rewards = np.array(self.rewards)
        self.log_prob = np.array(self.log_prob)
    
    def compute_weights(self):
        """Compute weights in the dataset"""
        hist, bin_edges = np.histogram(self.rewards, bins=[0, 1, 2, 3])
        hist = hist/ np.sum(hist)
        self.freq_count = hist

        temp = 0.5
        softmax_bins = softmax(bin_edges[:-1], temp=temp)
        self.softmax_bins = softmax_bins

        self.provable_dist = softmax_bins * self.freq_count/ (self.freq_count + 1e-3)
        self.provable_dist = self.provable_dist / np.sum(self.provable_dist)
    
    def get_weights(self):
        return {
            'hist': self.freq_count,
            'softmax_bins': self.softmax_bins,
            'provable_dist': self.provable_dist,
        }

    def __getitem__(self, index):
        img, target = super(BanditCIFAR, self).__getitem__(index)
        reward = self.rewards[index]
        y = self.chosen_targets[index]
        log_prob = self.log_prob[index]
        # print (y, reward)
        return img.type(torch.cuda.FloatTensor), torch.tensor(np.array(y)).type(torch.cuda.LongTensor), torch.Tensor(np.array(reward)).type(torch.cuda.FloatTensor), target,\
                torch.tensor(log_prob).type(torch.cuda.FloatTensor)


class BanditMNIST(MNIST):
    """MNIST modified for bandits"""
    def __init__(self, root, train=True, transform=None, target_transform=None, download=False, random_policy_type='uniform', weighting_scheme='uniform'):
        super(BanditMNIST, self).__init__(root=root, train=train, transform=transform, target_transform=target_transform, download=download)
        self.random_policy_type = random_policy_type
        self.num_targets = 10      

        self.rewards = []
        self.int_targets = []
        self.chosen_targets = []
        self.log_prob = []
        for idx, (img, target) in enumerate(zip(self.data, self.targets)):
            target = int(self.targets[idx])
            self.int_targets.append(target)
            if self.random_policy_type == 'biased':
                action = np.random.choice(10)
                log_prob = np.log(0.1)
            elif self.random_policy_type == 'uniform':
                prob_vec = 5.0/(self.num_targets - 1) * np.ones((self.num_targets,))
                prob_vec[target] = 5.0
                prob_vec = 1.0/self.num_targets * prob_vec
                action = np.random.choice(self.num_targets, p=prob_vec)
                log_prob = np.log(prob_vec[action])
            elif self.random_policy_type == 'oracle':
                action = target
                log_prob = 0.0
            elif self.random_policy_type == 'targeted':
                prob_vec = 5.0/(self.num_targets - 1) * np.ones((self.num_targets,))
                prob_vec[target] = 2.0
                prob_vec = 1.0/self.num_targets * prob_vec
                if target == '3':
                    prob_vec[8] = 0.0
                    prob_vec[9] = 0.0
                elif target == '8':
                    prob_vec[3] = 0.0
                    prob_vec[6] = 0.0
                elif target == '9':
                    prob_vec[6] = 0.0
                    prob_vec[8] = 0.0
                prob_vec = prob_vec * 1.0/np.sum(prob_vec)
                action = np.random.choice(self.num_targets, p=prob_vec)
                log_prob = np.log(prob_vec[action])
            else:
                raise NotImplementedError
            
            reward = np.clip(2.0 - 1.5*np.abs(action - target), 0.5, 2) # - 2.5
            self.rewards.append(reward)
            self.chosen_targets.append(one_hot(np.array([action,]), depth=self.num_targets,)[0])
            self.log_prob.append(log_prob)
        
        self.int_targets = np.array(self.int_targets)
        self.rewards = np.array(self.rewards)
        self.log_prob = np.array(self.log_prob)
            
    def compute_weights(self):
        """Compute weights in the dataset"""
        hist, bin_edges = np.histogram(self.rewards, bins=[0, 1, 2, 3])
        hist = hist/ np.sum(hist)
        self.freq_count = hist

        temp = 0.5
        softmax_bins = softmax(bin_edges[:-1], temp=temp)
        self.softmax_bins = softmax_bins

        self.provable_dist = softmax_bins * self.freq_count/ (self.freq_count + 1e-3)
        self.provable_dist = self.provable_dist / np.sum(self.provable_dist)
    
    def get_weights(self):
        return {
            'hist': self.freq_count,
            'softmax_bins': self.softmax_bins,
            'provable_dist': self.provable_dist,
        }

    def __getitem__(self, index):
        img, target = super(BanditMNIST, self).__getitem__(index)
        reward = self.rewards[index]
        y = self.chosen_targets[index]
        log_prob = self.log_prob[index]
        # print (y, reward)
        # print (img.shape, target)
        return img.type(torch.cuda.FloatTensor), torch.tensor(np.array(y)).type(torch.cuda.LongTensor), torch.Tensor(np.array(reward)).type(torch.cuda.FloatTensor), target,\
                torch.tensor(log_prob).type(torch.cuda.FloatTensor)
