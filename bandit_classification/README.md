# Model Inversion Nets: Code for Bandit Learning on MNIST
In this directory, we provide code for Bandit learning from partial feedback on MNIST. This experiment is provided in Section 4.1 of the paper.

## Requirements
We use basic requirements as [rlkit](https://github.com/vitchyr/rlkit), and the easiest way to install all requirements for running our code for this experiment is to install rlkit environment. We operate in a linux-gpu environment, with PyTorch-1.1 and Cuda 10. We would recommend installing the `linux-gpu` conda-env from rlkit for this experiment.

Also add the rlkit directory to PYTHONPATH:
```
export PYTHONPATH=/path/to/rlkit:$PYTHONPATH
```

## Running experiments
In order to run experiments, we use the following command:
```
python trainer.py --ngpu=<gpu-id> --add_fakes --disc_only --dataset='MNIST' --policy_type=<uniform/biased> --log_dir=<path/to/log/dir> --weighting_scheme='biased_softmax'
```

It takes about 300-400 epochs for MNIST with a dataset of 50000 bandit feedback examples (unlike supervised training examples). Walk clock times for training are roughly 10-12 hours on a GPU. In orcer to speed up training, set the evaluation using the inference procedure in Section 3.2 to False (else it takes a lot of time), and only perform it at the end of training. By default, we have turned it off (Line 226 of trainer.py), and setting `False` to `True` on Line 226 of trainer.py will turn it on each iteration.

## CommandLine Arguments/ Hyperparameters
The following are the commandline arguments for running MINs on these datasets.
1. `--add_fakes`[True/False]: Setting it to true helps GAN training, where the discriminator is also fed in wrongly labeled images that helps it discriminate and learn better. (This is different from randomized labelling in the paper, since this is a static dataset task and we are only using this is a GAN trick, also turning it off is not too bad in this setting.)
2. `--bce`: Whether to use binary cross entropy loss or the mean squared error loss, we find that not using BCE worked out better, (i.e. using mean squared error).
3. `--policy_type=<uniform/biased>`: The dataset type, in particular, this chooses the type of the logging policy that will generate the bandit logging dataset. 'uniform' refers to the case of 49% correct experiment and 'biased' refers to the case of random uniform over labels.
4. `weighting_scheme`: Weighting scheme to train the MIN model, set to `biased_softmax`, since we are maximizing the score here. 

