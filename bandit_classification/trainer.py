import torch
import numpy as np 
import os
import argparse
import min_model as model_inversion
import dataloader 
import torchvision
from torch.utils.data import DataLoader
import torch.distributions as td
import torchvision.transforms as transforms


# Logging
from logger import logger, setup_logger
from logger import create_stats_ordered_dict
from torch.autograd import Variable

parser = argparse.ArgumentParser(description='parser')
parser.add_argument("--num_dim", type=int, default=6, help="Number of dimensions of inputs in the process")
parser.add_argument("--disc_only", action='store_true', help="Discriminator only training of ")
parser.add_argument("--add_fakes", action='store_true', help='Add fakes or not')
parser.add_argument("--weighting_scheme", type=str, default="none", help="Adding weighting scheme for the model")
parser.add_argument("--lamda", type=float, default=1.0, help="default for lamda -- the fully connected reconstruction coefficient")
parser.add_argument("--nu", type=float, default=0.5, help="default for nu -- the fully connected prior coefficient")
parser.add_argument("--function_limit", type=float, default=-4.0, help="function limit on the lower side")
parser.add_argument("--ngpu", type=int, default=0, help="Gpu ids")
parser.add_argument("--policy_type", type=str, default='uniform', help='policy for collecting logged data')
parser.add_argument("--log_dir", type=str, default="./data/", help="Log directory")
parser.add_argument("--bce", action='store_true')
parser.add_argument("--dataset", type=str, default="CIFAR", help="dataset selected: CIFAR/MNIST")
log_dir = './data/'

device ='cuda'

def find_best_z(net, forward_net, y_start, img, num_itrs=100, lamda=0.2, nu=0.01, noise_dim=3):
    """Find the best possible z for a given x"""
    # import ipdb; ipdb.set_trace()
    y = Variable(y_start, requires_grad=False)
    z = torch.rand(y_start.shape[0], noise_dim, requires_grad=True, device='cuda')
    opt_y_z = torch.optim.Adam([z,], lr=0.001)
    std_normal = td.Normal(loc=0.0, scale=1.0)
    net.eval()

    for idx in range(num_itrs):
        with torch.set_grad_enabled(True):
            sampled_x = net.forward_tmp(score=y, img=img, noise=z)
            loss_t = -forward_net.get_log_prob(score=sampled_x, cond=img).mean() - nu * std_normal.log_prob(z).sum(-1).mean()
            
        opt_y_z.zero_grad()
        loss_t.backward()
        opt_y_z.step()
    # y_grad = torch.autograd.grad(loss_t, y, grad_outputs=torch.ones_like(y).cuda(), allow_unused=True, retain_graph=True)
    # z_grad = torch.autograd.grad(loss_t, z, grad_outputs=torch.ones_like(z).cuda(), allow_unused=True, retain_graph=False)

    # Sampling hard here to make sure that we get one hot vector
    final_out = net.forward_tmp(score=y, img=img, noise=z, sample_hard=True).detach().cpu().numpy()
    net.train()
    return final_out    


def get_datasets(args):
    if args['dataset'] == 'CIFAR':
        train_dataset = dataloader.BanditCIFAR(root=log_dir, train=True, download=True,
                            transform=torchvision.transforms.Compose([
                                transforms.RandomCrop(32, padding=4),
                                transforms.RandomHorizontalFlip(),
                                transforms.ToTensor(),
                                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                             ]), random_policy_type=args['policy_type'])
        test_dataset = dataloader.BanditCIFAR(root=log_dir, train=False, download=True,
                                transform=torchvision.transforms.Compose([
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                                ]), random_policy_type=args['policy_type'])
    else:
        train_dataset = dataloader.BanditMNIST(root=log_dir, train=True, download=True,
                                transform=torchvision.transforms.Compose([
                                torchvision.transforms.ToTensor(),
                                torchvision.transforms.Normalize(
                                    (0.5,), (0.5,))
                                ]), random_policy_type=args['policy_type'])
        test_dataset = dataloader.BanditMNIST(root=log_dir, train=False, download=True,
                                transform=torchvision.transforms.Compose([
                                torchvision.transforms.ToTensor(),
                                torchvision.transforms.Normalize(
                                    (0.5,), (0.5,))
                                ]), random_policy_type=args['policy_type'])
    return train_dataset, test_dataset


def get_dataloaders(train_dataset, test_dataset=None):
    train_loader = DataLoader(train_dataset, batch_size=128, shuffle=True)
    if test_dataset is not None:
        test_loader = DataLoader(test_dataset, batch_size=128, shuffle=True)
        return train_loader, test_loader
    else:
        return train_loader


def build_models_and_optimizers(args):
    if args['dataset'] == 'MNIST':
        cont_net = model_inversion.ModelInversionNetwithDiscriminator(input_dim=1, noise_dim=4, output_dim=10)
        cont_net_gen_opt = torch.optim.Adam(cont_net.gen_network.parameters(), lr=0.0002, betas=(0.5, 0.999))
        cont_net_disc_opt = torch.optim.Adam(cont_net.discriminator.parameters(), lr=0.0002, betas=(0.5, 0.999))

        # Forward model
        forward_model = model_inversion.ForwardModel(input_size=1, output_size=10, cond_size=784, num_res_layers=0)
        forward_opt = torch.optim.Adam(forward_model.parameters(), lr=0.001)
    
    elif args['dataset'] == 'CIFAR':
        cont_net = model_inversion.ConvContextualBandit(input_dim=1, noise_dim=8, output_dim=10,)
        cont_net_gen_opt = torch.optim.Adam(cont_net.gen_network.parameters(), lr=0.0002, betas=(0.5, 0.999))
        cont_net_disc_opt = torch.optim.Adam(cont_net.discriminator.parameters(), lr=0.0002, betas=(0.5, 0.999))

        forward_model = model_inversion.ConvForwardModel(num_blocks=[2,2,2,2], num_classes=10)
        forward_opt = torch.optim.Adam(forward_model.parameters(), lr=0.001)

    return cont_net, cont_net_gen_opt, cont_net_disc_opt, forward_model, forward_opt

def weight_fn(scores, train_dataset, args):
    # import ipdb; ipdb.set_trace()
    weights = train_dataset.get_weights()
    softmax_weights = weights['softmax_bins']
    provable_dist_weights = weights['provable_dist']
    hist_weights = weights['hist']

    scores = scores.cpu().numpy()
    bin_index = np.digitize(scores, bins=[0.0, 1.0, 2.0, 3.0])
    hist_prob = hist_weights[np.clip(bin_index-1, 0, 2)]
    provable_dist_prob = provable_dist_weights[np.clip(bin_index-1, 0, 2)]
    weights = provable_dist_prob / (hist_prob + 1e-7)
    weights = torch.Tensor(weights).type(torch.cuda.FloatTensor)
    weights = torch.clamp(weights, min=0.0, max=5.0)
    if args['weighting_scheme'] == 'none':
        weights = torch.ones_like(weights).type(torch.cuda.FloatTensor)
    return weights

def eval(net, batch, y_star):
    score = batch['score']
    # import ipdb; ipdb.set_trace()
    image = batch['image']
    location = batch['location']
    extrapolated_score = torch.ones_like(score).type(torch.cuda.FloatTensor) * 2.0
    with torch.no_grad():
        net.eval()
        out_actions = net(extrapolated_score, img=image, with_logits=True, sample_hard=True)
        net.train()
    
    out_actions = out_actions[0]
    out_actions = out_actions.cpu().detach().numpy()
    out_actions = np.argmax(out_actions, axis=-1)
    y_star = y_star.cpu().numpy()
    num_correct = np.equal(out_actions, y_star).sum()
    return {'correct': num_correct, 'total_examples' : score.shape[0]}

def eval_best_z(net, forward_net, args, batch, y_star):
    # import ipdb; ipdb.set_trace()
    score = batch['score']
    image = batch['image']
    location = batch['location']
    ystart = torch.ones_like(score).type(torch.cuda.FloatTensor) * 2.0
    out_actions = find_best_z(net, forward_net, ystart, image, num_itrs=200, lamda=args['lamda'], nu=args['nu'], noise_dim=4)
    # out_actions = out_actions.cpu().detach().numpy()

    out_actions = np.argmax(out_actions, axis=-1)
    y_star = y_star.cpu().numpy()
    num_correct = np.equal(out_actions, y_star).sum()
    return {'correct': num_correct, 'total_examples' : score.shape[0]}

def run_experiment(args):
    train_dataset, test_dataset = get_datasets(args)
    train_loader, test_loader = get_dataloaders(train_dataset, test_dataset)
    train_dataset.compute_weights()
    test_dataset.compute_weights()

    # Build models
    net, gen_opt, disc_opt, forward_model, forward_opt = build_models_and_optimizers(args)
    net.cuda()
    forward_model.cuda()

    # Set up weights function
    def _compute_weights(scores):
        return weight_fn(scores, train_dataset, args=args)

    n_epochs = 1000
    # import ipdb; ipdb.set_trace()

    for epoch in range(n_epochs):
        for x, y, r, y_star, _ in train_loader:
            batch = dict()
            batch['score'] = r.unsqueeze(-1).to(device)
            if args['dataset'] == 'MNIST':
                batch['image'] = x.view(x.shape[0], -1).to(device)
            else:
                batch['image'] = x.to(device)
            batch['location'] = y.to(device)
            weights = _compute_weights(r)
            gen_loss, disc_loss, fake_acc, real_acc = model_inversion.train_net_wd_with_weighting(net=net, optimizer_g=gen_opt, optimizer_d=disc_opt,
                                        batch=batch, weights=weights,
                                        add_fakes=args['add_fakes'], disc_only=args['disc_only'],
                                        weight_fn=_compute_weights,
                                        use_bce=args['bce'])    

            forward_loss = model_inversion.train_forward_net_with_weighting(net=forward_model, optimizer=forward_opt, batch=batch, weights=weights)

            # print ('Gen Loss: %.4f, Disc Loss: %.4f, Forward Loss: %.4f, Real Acc: %.4f, Fake Acc: %.4f, Mean Reward: %.4f' % (gen_loss.item(), disc_loss.item(), forward_loss.item(), real_acc, fake_acc, r.mean().item()))
            # break
        # Eval loop
        eval_accuracy = 0.0
        eval_total = 0.0
        eval_z_accuracy = 0.0
        for x, y, r, y_star, _ in test_loader:
            batch = dict()
            batch['score'] = r.unsqueeze(-1).to(device)
            if args['dataset'] == 'MNIST':
                batch['image'] = x.view(x.shape[0], -1).to(device)
            else:
                batch['image'] = x
            batch['location'] = y.to(device)
            dict_t = eval(net, batch, y_star)
            eval_accuracy += dict_t['correct']
            eval_total += dict_t['total_examples']

            # Performing extrapolated eval
            # This operation takes a lot of time for convnets
            if False:
                dict_t = eval_best_z(net, forward_model, args, batch, y_star)
                eval_z_accuracy += dict_t['correct']

            # print ('Correct: %.4f, Total: %.4f' % (dict_t['correct'], dict_t['total_examples']))

        overall_acc = eval_accuracy / eval_total
        overall_z_acc = eval_z_accuracy / eval_total
        logger.record_tabular('Accuracy', overall_acc)
        logger.record_tabular('Accuracy Z', overall_z_acc)
        logger.record_tabular('Gen Loss', gen_loss.item())
        logger.record_tabular('Disc Loss', disc_loss.item())
        logger.record_tabular('Disc Fake Acc', fake_acc.item())
        logger.record_tabular('Disc Real Acc', real_acc.item())
        logger.record_tabular('Forward Loss', forward_loss.item())
        logger.dump_tabular()

if __name__ == '__main__':
    opt = parser.parse_args()
    os.environ["CUDA_VISIBLE_DEVICES"] = str(opt.ngpu)
    
    seed = np.random.randint(100, 10000)
    variant = vars(opt)
    dir_name = "-".join([str(x)+ '=' + str(variant[x]) for x in variant.keys()]) + '-reset_false'
    print ('Logging to... ', dir_name)
    setup_logger(dir_name + '-' + str(seed), variant=variant, log_dir=(opt.log_dir + dir_name + '-' + str(seed)))

    # print ("ARGS: ", varuan)
    run_experiment(args=variant)
