import torch
import numpy as np 
import os
import argparse
import model  
import dataloader 
import torchvision
from torch.utils.data import DataLoader
import torch.distributions as td
import torchvision.transforms as transforms
import torch.nn.functional as F

# Logging
from logger import logger, setup_logger
from logger import create_stats_ordered_dict
from torch.autograd import Variable

parser = argparse.ArgumentParser(description='parser')
parser.add_argument("--objective_type", type=str, default="none", help="Objective type")
parser.add_argument("--ngpu", type=int, default=0, help="Gpu ids")
# parser.add_argument("--log_dir", type=str, default="./data/", help="Log directory")
parser.add_argument("--dataset", type=str, default="CIFAR", help="dataset selected: CIFAR/MNIST")
parser.add_argument("--policy_type", type=str, default="biased", help="Type of logging policy")
parser.add_argument("--experiment_name", type=str, default="None", help='experiment_name')
parser.add_argument("--entropy_coeff", type=float, default=0.05, help="entropy_coeff")
log_dir = './data_contextual_bandit/'

device ='cuda'

def get_datasets(args):
    if args['dataset'] == 'CIFAR':
        train_dataset = dataloader.BanditCIFAR(root=log_dir, train=True, download=True,
                            transform=torchvision.transforms.Compose([
                                transforms.RandomCrop(32, padding=4),
                                transforms.RandomHorizontalFlip(),
                                transforms.ToTensor(),
                                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                             ]), random_policy_type=args['policy_type'])
        test_dataset = dataloader.BanditCIFAR(root=log_dir, train=False, download=True,
                                transform=torchvision.transforms.Compose([
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                                ]), random_policy_type=args['policy_type'])
    else:
        train_dataset = dataloader.BanditMNIST(root=log_dir, train=True, download=True,
                                transform=torchvision.transforms.Compose([
                                torchvision.transforms.ToTensor(),
                                torchvision.transforms.Normalize(
                                    (0.5,), (0.5,))
                                ]), random_policy_type=args['policy_type'])
        test_dataset = dataloader.BanditMNIST(root=log_dir, train=False, download=True,
                                transform=torchvision.transforms.Compose([
                                torchvision.transforms.ToTensor(),
                                torchvision.transforms.Normalize(
                                    (0.5,), (0.5,))
                                ]), random_policy_type=args['policy_type'])
    return train_dataset, test_dataset

def get_dataloaders(train_dataset, test_dataset=None):
    train_loader = DataLoader(train_dataset, batch_size=128, shuffle=True)
    if test_dataset is not None:
        test_loader = DataLoader(test_dataset, batch_size=128, shuffle=True)
        return train_loader, test_loader
    else:
        return train_loader

def build_models_and_optimizers(args):
    if args['dataset'] == 'MNIST':
        net = model.FFNModel(input_dim=784, num_classes=10)
        net_opt = torch.optim.Adam(net.parameters(), lr=1e-4)

        if args['objective_type'] == 'minimaxCRM' or args['objective_type'] == 'minimax_relaxed_CRM':
            dist_net = model.FFNModel(input_dim=784, num_classes=10)
            dist_net_opt = torch.optim.Adam(dist_net.parameters(), lr=1e-4)

    elif args['dataset'] == 'CIFAR':
        net = model.Resnet18(num_classes=10)
        net_opt = torch.optim.Adam(net.parameters(), lr=0.0001)

        if args['objective_type'] == 'minimaxCRM' or args['objective_type'] == 'minimax_relaxed_CRM':
            dist_net = model.Resnet18(num_classes=10)
            dist_net_opt = torch.optim.Adam(dist_net.parameters(), lr=0.0001)

    if args['objective_type'] == 'minimaxCRM' or args['objective_type'] == 'minimax_relaxed_CRM':
        return net, net_opt, dist_net, dist_net_opt
    return net, net_opt

def objective(predictions, logging_prob, all_log_probs, reward, args, dist_probs=None):
    obj_type = args['objective_type']
    risk = torch.clamp(predictions - logging_prob, max=3).exp() * reward
    exp_risk = torch.clamp(predictions - logging_prob, max=3).exp() * reward.exp()
    log_imp_ratios = (predictions - logging_prob)
    entropy = -(all_log_probs * F.softmax(all_log_probs, dim=-1)).sum(-1)

    # import ipdb; ipdb.set_trace()
    if obj_type == 'CRM':
        var_risk = torch.std(risk)
        overall_loss = risk.mean() - 10.0 * var_risk + args['entropy_coeff'] * entropy.mean()
    elif obj_type == 'exp_CRM':
        var_risk = torch.std(exp_risk)
        overall_loss = exp_risk.mean() - 1.0 * var_risk + args['entropy_coeff'] * entropy.mean()
    elif obj_type == 'Dale':
        var_risk = torch.std(risk)
        overall_loss = (log_imp_ratios * reward).mean() - 0.0 * var_risk + args['entropy_coeff'] * entropy.mean()
    elif obj_type == 'exp_Dale':
        var_risk = torch.std(risk)
        overall_loss = (log_imp_ratios * torch.exp(reward)).mean() + args['entropy_coeff'] * entropy.mean()
    elif obj_type == 'minimaxCRM':
        # import ipdb; ipdb.set_trace()
        loss = dist_probs * risk
        overall_loss = loss.sum() + args['entropy_coeff'] * (entropy * dist_probs).sum()
        # overall_obj = all_log_probs * 
    elif obj_type == 'minimax_relaxed_CRM':
        pass
    return overall_loss, log_imp_ratios, entropy

def eval(net, batch, y_star):
    score = batch['score']
    image = batch['image']

    with torch.no_grad():
        net.eval()
        out_actions = net(image)
        net.train()
    
    # import ipdb; ipdb.set_trace()
    # out_actions = out_actions[0]
    out_actions = out_actions.cpu().detach().numpy()
    out_actions = np.argmax(out_actions, axis=-1)
    y_star = y_star.cpu().numpy()
    num_correct = np.equal(out_actions, y_star).sum()
    return {'correct': num_correct, 'total_examples' : score.shape[0]}

def run_experiment_minimax(args):
    train_dataset, test_dataset = get_datasets(args)
    train_loader, test_loader = get_dataloaders(train_dataset, test_dataset)

    net, net_opt, dist_net, dist_opt = build_models_and_optimizers(args)
    net.cuda()
    dist_net.cuda()
    n_epochs = 1000

    for epoch in range(n_epochs):
        for x, y, rew, y_star, logging_log_prob in train_loader:
            batch = dict()
            if args['dataset'] == 'MNIST':
                batch['image'] = x.view(x.shape[0], -1).to(device)
            else:
                batch['image'] = x.to(device)
            
            # predicted output for the net
            output = net(batch['image'])
            output_log_prob = F.log_softmax(output, dim=-1)
            y_idx = torch.argmax(y, -1)
            predicted_log_prob = output_log_prob[range(y.size(0)), y_idx]

            # Predicted output for the distribution: p(s, a) = p(s) * p(a|s) or directly p(s, a)
            dist_output = dist_net(batch['image'])
            dist_output_direct = dist_net(batch['image'])
            dist_output_pred = dist_output_direct[range(y.size(0)), y_idx]
            dist_value = torch.clamp(torch.exp(dist_output_pred), min=0, max=100)
            dist_probs = dist_value / torch.sum(dist_value, dim=-1, keepdim=True)

            empirical_risk, log_imp_ratios, entropy = objective(predicted_log_prob, logging_log_prob, output_log_prob, rew, args, 
                                                                dist_probs=dist_probs)

            # print ('Empirical Risk: %.4f, ImportanceMax: %.4f, ImportanceMean: %.4f, ProbsMax: %.4f, UnNormProbMax : %.4f' % (empirical_risk.mean().item(), 
            #                 log_imp_ratios.max().item(), log_imp_ratios.mean().item(), dist_probs.max().item(), dist_output_direct.abs().max().item()))
            # Backprop through both
            net_opt.zero_grad()
            (-empirical_risk).backward(retain_graph=True)
            net_opt.step()

            # Backprop through the other one: dist_net
            dist_opt.zero_grad()
            (empirical_risk).backward()
            dist_opt.step()
        
        # Eval loop
        eval_accuracy = 0.0
        eval_total = 0.0
        for x, y, r, y_star, _ in test_loader:
            batch = dict()
            batch['score'] = r.unsqueeze(-1).to(device)
            if args['dataset'] == 'MNIST':
                batch['image'] = x.view(x.shape[0], -1).to(device)
            else:
                batch['image'] = x

            dict_t = eval(net, batch, y_star)
            eval_accuracy += dict_t['correct']
            eval_total += dict_t['total_examples']

            # print ('Correct: %.4f, Total: %.4f' % (dict_t['correct'], dict_t['total_examples']))

        overall_acc = eval_accuracy / eval_total
        # overall_z_acc = eval_z_accuracy / eval_total
        logger.record_tabular('Accuracy', overall_acc)
        logger.record_tabular('Loss', empirical_risk.item())
        logger.record_dict(create_stats_ordered_dict(
            'Importance Ratios',
            log_imp_ratios.cpu().detach().numpy(),
        ))
        logger.record_dict(create_stats_ordered_dict(
            'Entropy',
            entropy.cpu().detach().numpy(),
        ))
        logger.dump_tabular()

def run_experiment(args):
    train_dataset, test_dataset = get_datasets(args)
    train_loader, test_loader = get_dataloaders(train_dataset, test_dataset)

    net, net_opt = build_models_and_optimizers(args)
    net.cuda()
    n_epochs = 1000

    for epoch in range(n_epochs):
        for x, y, rew, y_star, logging_log_prob in train_loader:
            batch = dict()
            if args['dataset'] == 'MNIST':
                batch['image'] = x.view(x.shape[0], -1).to(device)
            else:
                batch['image'] = x.to(device)
            
            # get predictions from model
            output = net(batch['image'])
            # import ipdb; ipdb.set_trace()
            output_log_prob = F.log_softmax(output, dim=-1)
            y_idx = torch.argmax(y, -1)
            predicted_log_prob = output_log_prob[range(y.size(0)), y_idx]

            # compute the loss
            empirical_risk, log_imp_ratios, entropy = objective(predicted_log_prob, logging_log_prob, output_log_prob, rew, args)
            
            # Backprop
            net_opt.zero_grad()
            (-empirical_risk).backward()
            net_opt.step()

        # Eval loop
        eval_accuracy = 0.0
        eval_total = 0.0
        for x, y, r, y_star, _ in test_loader:
            batch = dict()
            batch['score'] = r.unsqueeze(-1).to(device)
            if args['dataset'] == 'MNIST':
                batch['image'] = x.view(x.shape[0], -1).to(device)
            else:
                batch['image'] = x

            dict_t = eval(net, batch, y_star)
            eval_accuracy += dict_t['correct']
            eval_total += dict_t['total_examples']

            # print ('Correct: %.4f, Total: %.4f' % (dict_t['correct'], dict_t['total_examples']))

        overall_acc = eval_accuracy / eval_total
        # overall_z_acc = eval_z_accuracy / eval_total
        logger.record_tabular('Accuracy', overall_acc)
        logger.record_tabular('Loss', empirical_risk.item())
        logger.record_dict(create_stats_ordered_dict(
            'Importance Ratios',
            log_imp_ratios.cpu().detach().numpy(),
        ))
        logger.record_dict(create_stats_ordered_dict(
            'Entropy',
            entropy.cpu().detach().numpy(),
        ))
        logger.dump_tabular()

if __name__ == '__main__':
    opt = parser.parse_args()
    os.environ["CUDA_VISIBLE_DEVICES"] = str(opt.ngpu)
    
    seed = np.random.randint(100, 10000)

    variant = vars(opt)
    dir_name = variant['experiment_name']
    print ('Logging to... ', dir_name)
    setup_logger(dir_name + '-' + str(seed), variant=variant, log_dir=(log_dir + dir_name + '-' + str(seed)))

    if variant['objective_type'] == 'minimaxCRM' or variant['objective_type'] == 'minimax_relaxed_CRM':
        run_experiment_minimax(args=variant)
    else:
        run_experiment(args=variant)