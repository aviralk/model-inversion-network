import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import datetime as dt
import torchvision
import time
from tensorboardX import SummaryWriter
writer = None
import argparse

from model import FFNModel, ConvModel
from torch.utils.data import DataLoader
import dataloader 
from eval import evaluate
from model import CustomLoss, LikelihoodLoss
from utils import *
import torchvision.transforms as transforms

parser = argparse.ArgumentParser(description='parser')
parser.add_argument("--dataset", type=str, default="CIFAR", help='dataset to run model on')
parser.add_argument("--risk_type", type=str, default="snips", help="risk type")
opt = parser.parse_args()

def get_datasets(log_dir=None, policy_type='biased'):
    if opt.dataset == 'CIFAR':
        train_dataset = dataloader.BanditCIFAR(root=log_dir, train=True, download=True,
                            transform=torchvision.transforms.Compose([
                                transforms.RandomCrop(32, padding=4),
                                transforms.RandomHorizontalFlip(),
                                transforms.ToTensor(),
                                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                             ]), random_policy_type=policy_type)
        test_dataset = dataloader.BanditCIFAR(root=log_dir, train=False, download=True,
                                transform=torchvision.transforms.Compose([
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                                ]), random_policy_type=policy_type)
    else:
        train_dataset = dataloader.BanditMNIST(root=log_dir, train=True, download=True,
                                transform=torchvision.transforms.Compose([
                                torchvision.transforms.ToTensor(),
                                torchvision.transforms.Normalize(
                                    (0.5,), (0.5,))
                                ]), random_policy_type=policy_type)
        test_dataset = dataloader.BanditMNIST(root=log_dir, train=False, download=True,
                                transform=torchvision.transforms.Compose([
                                torchvision.transforms.ToTensor(),
                                torchvision.transforms.Normalize(
                                    (0.5,), (0.5,))
                                ]), random_policy_type=policy_type)
    return train_dataset, test_dataset

def get_dataloaders(train_dataset, test_dataset=None):
    train_loader = DataLoader(train_dataset, batch_size=128, shuffle=True)
    if test_dataset is not None:
        test_loader = DataLoader(test_dataset, batch_size=128, shuffle=True)
        return train_loader, test_loader
    else:
        return train_loader

def train_likelihood_risk(model, criterion, optimizer, reader, hyper_params):
    model.train()
    metrics = {}
    total_batches = 0.0
    total_loss = FloatTensor([0.0])
    correct, total = LongTensor([0]), 0.0
    control_variate = FloatTensor([0.0])
    ips = FloatTensor([0.0])

    for x, y, delta, target, log_prob in reader:
        if opt.dataset == 'MNIST':
            output = model(x.reshape(x.shape[0], -1))
        else:
            output = model(x)

        output = F.log_softmax(output, dim=-1)
        
        loss = criterion(output, y, delta, torch.exp(log_prob))
        model.zero_grad()

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        writer.add_scalar('train loss', loss.data, total_batches)

        # Metrics
        prob_out = F.softmax(output, dim=-1) 
        total_loss += loss.data
        y = torch.argmax(y, -1)
        prop = torch.exp(log_prob)
        control_variate += torch.mean(prob_out[range(y.size(0)), y] / prop).data
        ips += torch.mean((delta * output[range(y.size(0)), y]) / prop).data
        _, predicted = torch.max(output.data, 1)
        total += y.size(0)
        correct += predicted.cpu().eq(target.data).sum().data
        total_batches += 1.0
        # print ('Correct: ', correct)
        
    metrics['loss'] = round(float(total_loss) / total_batches, 4)
    metrics['Acc'] = round(100.0 * float(correct) / float(total), 4)
    metrics['CV'] = round(float(control_variate) / total_batches, 4)
    metrics['SNIPS'] = round(float(ips) / float(control_variate), 4)
    
    return metrics
        
    
def train(model, criterion, optimizer, reader, hyper_params):
    model.train()
    metrics = {}
    total_batches = 0.0
    total_loss = FloatTensor([ 0.0 ])
    correct, total = LongTensor([ 0 ]), 0.0
    control_variate = FloatTensor([ 0.0 ])
    ips = FloatTensor([ 0.0 ])

    for x, y, delta, target, log_prob in reader:
        
        # import ipdb; ipdb.set_trace() 
        # Forward pass
        if opt.dataset == 'MNIST':
            output = model(x.reshape(x.shape[0], -1))
        else:
            output = model(x)
        
        output = F.softmax(output, dim=-1)
        
        # Backward pass
        loss = criterion(output, y, delta, torch.exp(log_prob))
        # Empty the gradients
        model.zero_grad()
        optimizer.zero_grad()
   
        loss.backward()
        optimizer.step()

        # Log to tensorboard
        writer.add_scalar('train loss', loss.data, total_batches)

        # Metrics evaluation
        total_loss += loss.data
        y = torch.argmax(y, -1)
        prop = torch.exp(log_prob)
        control_variate += torch.mean(output[range(y.size(0)), y] / prop).data
        ips += torch.mean((delta * output[range(y.size(0)), y]) / prop).data
        _, predicted = torch.max(output.data, 1)
        total += y.size(0)
        correct += predicted.cpu().eq(target.data).sum().data
        total_batches += 1.0
        # print ('Correct: ', correct)
        
    metrics['loss'] = round(float(total_loss) / total_batches, 4)
    metrics['Acc'] = round(100.0 * float(correct) / float(total), 4)
    metrics['CV'] = round(float(control_variate) / total_batches, 4)
    metrics['SNIPS'] = round(float(ips) / float(control_variate), 4)

    return metrics

def main(hyper_params=None, return_model=False):
    # If custom hyper_params are not passed, load from hyper_params.py
    if hyper_params is None: 
        from hyper_params import hyper_params
    else: 
        print("Using passed hyper-parameters..")

    # import ipdb; ipdb.set_trace()
    # Initialize a tensorboard writer
    global writer
    path = hyper_params['tensorboard_path']
    writer = SummaryWriter(path)

    # Train It..
    # train_reader, test_reader, val_reader = load_data(hyper_params)
    train_dataset, test_dataset = get_datasets(log_dir="./data", policy_type='biased')
    train_loader, test_loader = get_dataloaders(train_dataset, test_dataset)

    # file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
    # file_write(hyper_params['log_file'], "Data reading complete!")
    # file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(len(train_dataset)))
    # file_write(hyper_params['log_file'], "Number of test batches: {:4d}".format(len(test_dataset)))

    if opt.dataset == 'CIFAR':
        model = ConvModel(hyper_params)
    else:
        model = FFNModel()
    
    if is_cuda_available: 
        model.cuda()

    if opt.risk_type == 'snips':
        criterion = CustomLoss(hyper_params)
    else:
        criterion = LikelihoodLoss(hyper_params)

    optimizer = torch.optim.Adam(
        model.parameters(), lr=1e-3, weight_decay=hyper_params['weight_decay']
    )

    # optimizer = torch.optim.SGD(
    #     model.parameters(), lr=hyper_params['lr'], momentum=0.9, weight_decay=hyper_params['weight_decay']
    # )

    # file_write(hyper_params['log_file'], str(model))
    # file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

    best_metrics_train = None
    best_metrics_test = None

    try:
        for epoch in range(1, hyper_params['epochs'] + 1):
            epoch_start_time = time.time()
            
            # Training for one epoch
            if opt.risk_type == 'snips':
                metrics = train(model, criterion, optimizer, train_loader, hyper_params)
            else:
                metrics = train_likelihood_risk(model, criterion, optimizer, train_loader, hyper_params)
            
            string = ""
            for m in metrics: 
                string += " | " + m + ' = ' + str(metrics[m])
            string += ' (TRAIN)'

            best_metrics_train = metrics

            # Calulating the metrics on the validation set
            metrics = evaluate(model, criterion, test_loader, hyper_params, dataset=opt.dataset)
            string2 = ""
            for m in metrics: 
                string2 += " | " + m + ' = ' + str(metrics[m])
            string2 += ' (TEST)'

            best_metrics_test = metrics

            ss  = '-' * 89
            ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
            ss += string
            ss += '\n'
            ss += '-' * 89
            ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
            ss += string2
            ss += '\n'
            ss += '-' * 89
            # file_write(hyper_params['log_file'], ss)
            print (ss)
            
            for metric in metrics: 
                writer.add_scalar('Test_metrics/' + metric, metrics[metric], epoch - 1)
            
    except KeyboardInterrupt: print('Exiting from training early')

    writer.close()

    if return_model == True: return model
    return best_metrics_train, best_metrics_test

if __name__ == '__main__':
    main()
