import numpy as np
import torch
import argparse
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

from torch import distributions as td
import torch.nn.functional as F
import torch.nn as nn

device = 'cpu'
if torch.cuda.is_available():
  device = 'cuda'

def softmax(arr, temp=1.0):
  max_arr = arr.max()
  arr_new = arr - max_arr
  exp_arr = np.exp(arr_new/temp)
  return exp_arr / np.sum(exp_arr)

def softmin(arr, temp=1.0):
  return softmax(-arr, temp=temp)

def weighted_softmax(arr, weights, temp=1.0):
  max_arr = arr.max()
  arr_new = arr - max_arr
  exp_arr = np.exp(arr_new/temp)
  wt_exp_arr = exp_arr * weights
  return wt_exp_arr/np.sum(wt_exp_arr)

def weighted_softmin(arr, weights, temp=1.0):
  return weighted_softmax(-arr, weights, temp=1.0)

def adaptive_temp_v2(scores_np, base_temp=1.0):
    if len(scores_np) <= 5:
        return base_temp
    else:
        inverse_arr = -scores_np
        max_score = inverse_arr.max()
        scores_new = inverse_arr - max_score
        quantile_ninety = np.quantile(scores_new, q=0.9)
        return np.abs(quantile_ninety)

class ResnetFC(torch.nn.Module):
    def __init__(self, input_size, output_size, cond_size, num_res_layers, gumbel_softmax=True, tau=0.75):
        super(ResnetFC, self).__init__()
        self.input_dim = input_size
        self.output_size = output_size
        self.cond_size = cond_size
        self.linear1 = torch.nn.Linear(input_size, 256)
        self.linear1_cond = torch.nn.Linear(cond_size, 256)
        self.linear2 = torch.nn.Linear(512, 256)
        self.linear3 = torch.nn.Linear(256, self.output_size)
        self.bn1 = torch.nn.BatchNorm1d(512, 0.8)
        self.bn2 = torch.nn.BatchNorm1d(256, 0.8)
        # self.bn3 = torch.nn.BatchNorm1d(self.seq_dim * 20)
        self.relu = torch.nn.LeakyReLU(0.2, inplace=True)
        self.gumbel_softmax = gumbel_softmax
        self.tau = tau
    
    def forward(self, x, cond):
        x = self.linear1(x)
        cond = self.linear1_cond(cond)
        x = torch.cat([x, cond], -1)
        x = self.bn1(x)
        x = self.relu(x)

        # Now the linear layers
        x = self.linear2(x)
        x = self.bn2(x)
        x = self.relu(x)

        # The final linear layer
        x = self.linear3(x)
        # x = x.reshape(x.shape[0], self.seq_dim, self.output_size)
        logits = x

        if self.gumbel_softmax:
            uniform_noise = td.Uniform(low=0.0, high=1.0).sample(logits.shape).to(device)
            gumbel_softmax_noise = -(-(uniform_noise + 1e-8).log() + 1e-8).log()
            g = 1.0/self.tau * (logits + gumbel_softmax_noise)
            return torch.nn.Softmax(-1)(g)

        return logits

class DiscriminatorFC(torch.nn.Module):
    """Fully connected discriminator instead of conv1d"""
    def __init__(self, input_size, output_size, cond_size, num_res_layers,):
        super(DiscriminatorFC, self).__init__()
        self.input_dim = input_size
        self.num_res_layers = num_res_layers
        self.output_size = output_size
        self.cond_size = cond_size
        
        self.linear_score = torch.nn.Linear(1, 128)
        self.linear_inp = torch.nn.Linear(10, 128)
        self.linear_cond = torch.nn.Linear(self.cond_size, 256)
        self.linear_intermed = torch.nn.Linear(512, 256)
        self.linear_out = torch.nn.Linear(256, 1)
        self.relu = torch.nn.LeakyReLU(0.2, inplace=True)
        self.bn1 = torch.nn.BatchNorm1d(512)
        self.bn2 = torch.nn.BatchNorm1d(256)
        self.dropout = torch.nn.Dropout(0.4)
    
    def forward(self, score, x, cond):
        x = self.linear_inp(x)
        x = self.relu(x)

        score_t = self.linear_score(score)
        score_t = self.relu(score_t)

        cond_t = self.linear_cond(cond)
        cond_t = self.relu(cond_t)

        x = torch.cat([x, score_t, cond_t], -1)
        x = self.linear_intermed(x)
        # x = self.bn2(x)
        x = self.relu(x)
        x = self.linear_out(x)
        return x

def one_hot_v3(batch,depth):
    emb = torch.nn.Embedding(depth, depth)
    emb.weight.data = torch.eye(depth, device=device)
    return emb(batch)

# class ForwardModel(torch.nn.Module):
#     def __init__(self, input_size, output_size, cond_size, num_res_layers,):
#         super(ForwardModel, self).__init__()
#         self.input_dim = input_size
#         self.num_res_layers = num_res_layers
#         self.output_size = output_size
#         self.cond_size = cond_size
        
#         self.linear_score = torch.nn.Linear(1, 128)
#         self.linear_cond = torch.nn.Linear(self.cond_size, 384)
#         self.linear_intermed = torch.nn.Linear(512, 256)
#         self.linear_out = torch.nn.Linear(256, 256)
#         self.linear_final = torch.nn.Linear(256, 10)
#         self.relu = torch.nn.ReLU()
#         self.bn1 = torch.nn.BatchNorm1d(512)
#         self.bn2 = torch.nn.BatchNorm1d(256)
    
#     def forward(self, score, cond):
#         # import ipdb; ipdb.set_trace()
#         score_t = self.linear_score(score)
#         score_t = self.relu(score_t)

#         cond_t = self.linear_cond(cond)
#         cond_t = self.relu(cond_t)

#         x = torch.cat([score_t, cond_t], -1)
#         x = self.linear_intermed(x)
#         # x = self.bn2(x)
#         x = self.relu(x)
#         x = self.linear_out(x)
#         x = self.relu(x)

#         x = self.linear_final(x)
#         return x
    
#     # def get_log_prob(self, score, cond, action):

class ForwardModel(torch.nn.Module):
    def __init__(self, input_size, output_size, cond_size, num_res_layers,):
        super(ForwardModel, self).__init__()
        self.input_dim = input_size
        self.num_res_layers = num_res_layers
        self.output_size = output_size
        self.cond_size = cond_size
        
        self.linear_score = torch.nn.Linear(10, 128)
        self.linear_cond = torch.nn.Linear(self.cond_size, 384)
        self.linear_intermed = torch.nn.Linear(512, 256)
        self.linear_out = torch.nn.Linear(256, 256)
        self.linear_final = torch.nn.Linear(256, 3)
        self.relu = torch.nn.ReLU()
        self.bn1 = torch.nn.BatchNorm1d(512)
        self.bn2 = torch.nn.BatchNorm1d(256)
    
    def forward(self, score, cond):
        # import ipdb; ipdb.set_trace()
        score_t = self.linear_score(score)
        score_t = self.relu(score_t)

        cond_t = self.linear_cond(cond)
        cond_t = self.relu(cond_t)

        x = torch.cat([score_t, cond_t], -1)
        x = self.linear_intermed(x)
        # x = self.bn2(x)
        x = self.relu(x)
        x = self.linear_out(x)
        x = self.relu(x)

        x = self.linear_final(x)
        return x
    
    def get_log_prob(self, score, cond):
        logits = self.forward(score, cond)
        log_prob = torch.nn.LogSoftmax(dim=-1)(logits)
        # Assuming 3 in size
        return logits[:, 2]


class ConvForwardModel(nn.Module):
    def __init__(self, block=None, num_blocks=[2,2,2,2], num_classes=10):
        super(ConvForwardModel, self).__init__()
        self.in_planes = 64

        self.score_embed = nn.Linear(10, 256)
        self.conv1 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        if block is None:
            block = BasicBlock
        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], stride=2)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], stride=2)
        self.linear = nn.Linear(512*block.expansion, num_classes)
        self.score_linear = nn.Linear(256, 512*block.expansion)

    def _make_layer(self, block, planes, num_blocks, stride):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x, score):
        # import ipdb; ipdb.set_trace()
        # embed score:  
        score_embed = self.score_embed(score)
        score_embed = F.relu(score_embed)
        score_transformed = self.score_linear(score_embed)

        out = F.relu(self.bn1(self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)

        # Changing forward model
        out = out * score_transformed

        out = self.linear(out)
        return out
    
    def get_log_prob(self, score, cond):
        logits = self.forward(score=score, x=cond)
        log_prob = torch.nn.LogSoftmax(dim=-1)(logits)
        return logits[:, 2]

class ModelInversionNetwithDiscriminator(torch.nn.Module):
    def __init__(self, input_dim, noise_dim, output_dim, fully_connected=True, gumbel_softmax=True, tau=0.75, **kwargs):
        super(ModelInversionNetwithDiscriminator, self).__init__()
        self.inp_dim = input_dim
        self.output_dim = output_dim
        self.gumbel_softmax = gumbel_softmax
        self.tau = tau
        self.noise_dim = noise_dim
        self.fully_connected = fully_connected
        self.cond_size = 784
        
        self.gen_network = ResnetFC(input_size=self.inp_dim + self.noise_dim, output_size=self.output_dim, cond_size=self.cond_size,
                            num_res_layers=3, gumbel_softmax=self.gumbel_softmax, tau=self.tau)
        
        self.discriminator = DiscriminatorFC(input_size=self.output_dim, output_size=self.inp_dim,
                                cond_size=self.cond_size, num_res_layers=3)

    def forward(self, score, img, with_logits=False, sample_hard=False):
        random_noise = td.Normal(loc=0.0, scale=1.0).sample(sample_shape=(score.shape[0], self.noise_dim))
        input_in = torch.cat([score.type(torch.cuda.FloatTensor), random_noise.type(torch.cuda.FloatTensor)], -1)# .to(device)
        img = img.view(img.shape[0], -1)
        output = self.gen_network(input_in, cond=img)  # these are logits
        
        if self.gumbel_softmax:
            if sample_hard:
                hard_out = one_hot_v3(torch.max(output, -1)[1], depth=10)
                return hard_out, hard_out
            return output, output     # Hack
        
        dist = td.Categorical(logits=output)
        sample_softmax = dist.sample()
        logits = dist.log_prob(sample_softmax)
        sample_softmax = one_hot_v3(sample_softmax, depth=10).type(torch.cuda.FloatTensor)
        if with_logits:
            return sample_softmax, logits
        return sample_softmax

    def reset_weights(self):
        for layer in self.gen_network:
            if hasattr(layer, 'reset_parameters'):
                layer.reset_parameters()
            
        for layer in self.discriminator:
            if hasattr(layer, 'reset_parameters'):
                layer.reset_parameters()

    def set_weights_to_other(self, other_net):
        for w, target in zip(self.gen_network.parameters(), other_net.gen_network.parameters()):
            w.data = target.data

        for w, target in zip(self.discriminator.parameters(), other_net.discriminator.parameters()):
            w.data = target.data
  
    def discriminator_forward(self, score, point, img):
        # import ipdb; ipdb.set_trace()
        validity = self.discriminator(score.type(torch.cuda.FloatTensor), point.type(torch.cuda.FloatTensor), cond=img)
        return validity
    
    def forward_tmp(self, score, img, noise, sample_hard=False):
        input_in = torch.cat([score.type(torch.cuda.FloatTensor), noise.type(torch.cuda.FloatTensor)], -1)# .to(device)
        img = img.view(img.shape[0], -1)
        output = self.gen_network(input_in, cond=img)  # these are logits
        
        if self.gumbel_softmax:
            if sample_hard:
                hard_out = one_hot_v3(torch.max(output, -1)[1], depth=10)
                return hard_out
            return output

# For CIFAR model comparisons, we need to make the general output one hot vectors
# Generator doesn't output tanh, so that it can output labels
class BasicBlock(torch.nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, stride=1, relu=None):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion*planes)
            )
        
        self.relu = relu
        if relu is None:
            self.relu = F.relu

    def forward(self, x):
        out = self.relu(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        out = self.relu(out)
        return out


class Bottleneck(torch.nn.Module):
    expansion = 4

    def __init__(self, in_planes, planes, stride=1, relu=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, self.expansion*planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(self.expansion*planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion*planes)
            )
        
        self.relu = relu

    def forward(self, x):
        out = self.relu(self.bn1(self.conv1(x)))
        out = self.relu(self.bn2(self.conv2(out)))
        out = self.bn3(self.conv3(out))
        out += self.shortcut(x)
        out = self.relu(out)
        return out


class ContextualResNetGenerator(torch.nn.Module):
    def __init__(self, block, num_blocks, num_classes=10, embed_size=256, gumbel_softmax=True, tau=0.75):
        super(ContextualResNetGenerator, self).__init__()
        self.in_planes = 64

        def _relu(x):
            return F.leaky_relu(x, 0.2, inplace=True)
        
        self._relu = _relu
        # Modified classifier architecture
        self.z_conv = nn.Linear(embed_size, 512*block.expansion)
        self.conv1 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], stride=2)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], stride=2)
        self.score_embed = nn.Linear(1, embed_size//2)
        self.linear = nn.Linear(512*block.expansion, num_classes)

        self.gumbel_softmax = gumbel_softmax
        self.tau = tau

    def _make_layer(self, block, planes, num_blocks, stride):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride, relu=self._relu))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x, z, score):
        # import ipdb; ipdb.set_trace()
        score_embed = self.score_embed(score)

        # This helps it with the inductive bias of getting more disentagled vectors
        # score_embed = score_embed / torch.norm(score_embed, p=2, dim=1, keepdim=True)
        z_new = torch.cat([z, score_embed], -1)
        z_embed = F.relu(self.z_conv(z_new))

        out = F.leaky_relu(self.bn1(self.conv1(x)), 0.2, inplace=True)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)

        # Concat score and z values
        out = F.leaky_relu(out * z_embed, 0.2, inplace=True)
        # out = torch.cat([out, z_embed], -1)

        out = self.linear(out)

        if self.gumbel_softmax:
            uniform_noise = td.Uniform(low=0.0, high=1.0).sample(out.shape).to(device)
            gumbel_softmax_noise = -(-(uniform_noise + 1e-8).log() + 1e-8).log()
            g = 1.0/self.tau * (out + gumbel_softmax_noise)
            return torch.nn.Softmax(-1)(g)

        return out

class ContextualResNetDiscriminator(torch.nn.Module):
    def __init__(self, block, num_blocks, num_classes=10, embed_size=256):
        super(ContextualResNetDiscriminator, self).__init__()
        self.in_planes = 64

        self.relu = F.leaky_relu

        self.z_conv = nn.Linear(embed_size, 512*block.expansion)
        self.conv1 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], stride=2)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], stride=2)
        self.score_embed = nn.Linear(1, embed_size // 2)
        self.label_embed = nn.Linear(num_classes, embed_size // 2)
        self.linear = nn.Linear(512*block.expansion, 512*block.expansion)
        self.linear_out = nn.Linear(512*block.expansion, 1)
        
    def _make_layer(self, block, planes, num_blocks, stride):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride, relu=self.relu))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x, label, score):
        score_embed = self.score_embed(score)
        label_embed = self.label_embed(label)
        # This makes the model independent of the score value but just the direction
        # score_embed = score_embed / torch.norm(score_embed, p=2, dim=1, keepdim=True)
        z = torch.cat([score_embed, label_embed], -1)
        # z_new = torch.cat([z, score_embed], -1)
        z_embed = F.relu(self.z_conv(z))

        out = F.relu((self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)

        # Now we will add in the layers
        out = F.relu(out * z_embed)
        out = self.linear_out(out)
        # out = self.linear_out(out)
        return out

class ConvContextualBandit(torch.nn.Module):
    def __init__(self, input_dim, noise_dim, output_dim, gumbel_softmax=True, tau=0.75, cond_size=(32, 32, 3), **kwargs):
        super(ConvContextualBandit, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.gumbel_softmax = gumbel_softmax
        self.tau = tau
        self.noise_dim = noise_dim
        self.fully_connected = False
        self.cond_size = cond_size

        self.gen_network = ContextualResNetGenerator(BasicBlock, num_blocks=[2,2,2,2], num_classes=10, embed_size=self.noise_dim*2, gumbel_softmax=self.gumbel_softmax, tau=self.tau)
        self.discriminator = ContextualResNetDiscriminator(BasicBlock, num_blocks=[2,2,2,2], num_classes=10, embed_size=self.noise_dim*2)
    
    def forward(self, score, img, with_logits=False, sample_hard=False):
        # import ipdb; ipdb.set_trace()
        random_noise = td.Normal(loc=0.0, scale=1.0).sample(sample_shape=(score.shape[0], self.noise_dim))
        random_noise = random_noise.type(torch.cuda.FloatTensor)
        output = self.gen_network(x=img, z=random_noise, score=score)  # these are logits

        if self.gumbel_softmax:
            if sample_hard:
                hard_out = one_hot_v3(torch.max(output, -1)[1], depth=10)
                return hard_out, hard_out
            return output, output
    
        dist = td.Categorical(logits=output)
        sample_softmax = dist.sample()
        logits = dist.log_prob(sample_softmax)
        sample_softmax = one_hot_v3(sample_softmax, depth=10).type(torch.cuda.FloatTensor)
        if with_logits:
            return sample_softmax, logits
        return sample_softmax
    
    def reset_weights(self):
        for layer in self.gen_network:
            if hasattr(layer, 'reset_parameters'):
                layer.reset_parameters()
            
        for layer in self.discriminator:
            if hasattr(layer, 'reset_parameters'):
                layer.reset_parameters()
    
    def set_weights_to_other(self, other_net):
        for w, target in zip(self.gen_network.parameters(), other_net.gen_network.parameters()):
            w.data = target.data

        for w, target in zip(self.discriminator.parameters(), other_net.discriminator.parameters()):
            w.data = target.data
  
    def discriminator_forward(self, score, point, img):
        # import ipdb; ipdb.set_trace()
        validity = self.discriminator(score=score.type(torch.cuda.FloatTensor), label=point.type(torch.cuda.FloatTensor), x=img)
        return validity
    
    def forward_tmp(self, score, img, noise, sample_hard=False):
        input_in = torch.cat([score.type(torch.cuda.FloatTensor), noise.type(torch.cuda.FloatTensor)], -1)# .to(device)
        output = self.gen_network(x=img, score=score, z=noise) 

        if self.gumbel_softmax:
            if sample_hard:
                hard_out = one_hot_v3(torch.max(output, -1)[1], depth=10)
                return hard_out
            return output

def actvn(x):
    out = F.leaky_relu(x, 2e-1)
    return out

def reparametrize(mu, logstd):
    std = torch.exp(logstd)
    eps = torch.randn_like(std)
    return eps.mul(std) + mu

def get_query_point(X, y, net, scheme='softmax_extrapolated'):
    """query the next point to sample"""
    current_scores = y[:, 0]
    current_scores_np = current_scores.detach().cpu().numpy()
    if scheme == 'softmax_extrapolated':
        base_temp = 0.05
        base_temp = adaptive_temp_v2(current_scores_np, base_temp)
        softmin_score = softmax(current_scores_np, temp=base_temp)
        sample_softmin = np.random.choice(current_scores_np, p=softmin_score)
        random_noise = np.abs((np.random.normal(loc=0.0, scale=0.3)))
        score_query = np.maximum(sample_softmin - random_noise, -4.0)
    elif scheme == 'softmax':
        base_temp = 0.05
        base_temp = adaptive_temp_v2(current_scores_np, base_temp)
        softmin_score = softmax(current_scores_np, temp=base_temp)
        sample_softmin = np.random.choice(current_scores_np, p=softmin_score)
        score_query = sample_softmin
    
    score_query = torch.tensor(score_query)
    score_query = score_query.unsqueeze(0).unsqueeze(0)    # 1 x 1
    with torch.no_grad():
        net.eval()
        out_point = net(score_query)
        net.train()
    return out_point, score_query

def one_hot(nparray, depth=0, on_value=1, off_value=0):
    if depth == 0:
        depth = np.max(nparray) + 1
    assert np.max(nparray) < depth, "the max index of nparray: {} is larger than depth: {}".format(np.max(nparray), depth)
    shape = nparray.shape
    out = np.ones((*shape, depth)) * off_value
    indices = []
    for i in range(nparray.ndim):
        tiles = [1] * nparray.ndim
        s = [1] * nparray.ndim
        s[i] = -1
        r = np.arange(shape[i]).reshape(s)
        if i > 0:
            tiles[i-1] = shape[i-1]
            r = np.tile(r, tiles)
        indices.append(r)
    indices.append(nparray)
    out[tuple(indices)] = on_value
    return out

def perturb_epsilon(fake_data_np, epsilon=0.1):
    """Perturb the datapoints here"""
    current_one_hot = np.argmax(fake_data_np, -1)
    random_perturb = np.random.randint(0, 20, size=current_one_hot.shape)
    bernoulli_probs = np.random.binomial(1, p=epsilon, size=current_one_hot.shape)
    final_one_hot = current_one_hot * bernoulli_probs + (1.0 - bernoulli_probs) * random_perturb
    final_one_hot = final_one_hot.astype('int32')
    return one_hot(final_one_hot, depth=20)

################## TRAINING scripts #############################
def weighted_mse_loss(input, target, weight):
    return torch.mean(weight * (input - target) ** 2)

def weighted_bce_loss(input, target, weight):
    bce_loss = torch.nn.BCEWithLogitsLoss(reduction='none')
    loss_val = bce_loss(input, target)
    return (loss_val * weight).mean()

def get_random_score(score_init):
    batch_size = score_init.shape[0]
    return torch.clamp(score_init.type(torch.cuda.FloatTensor), min=0.0) + torch.randn(*score_init.shape).type(torch.cuda.FloatTensor)*0.4

def train_net_wd_with_weighting(net, optimizer_g, optimizer_d, batch, weights, add_fakes=False, disc_only=True, weight_fn=None, use_bce=False):
    """Train model inversion net with discriminator via weighting"""
    output, log_prob = net(batch['score'].to(device), img=batch['image'], with_logits=True)
    validity = net.discriminator_forward(batch['score'].to(device), output, img=batch['image'])

    disc_gen_loss = weighted_mse_loss
    if use_bce:
        disc_gen_loss = weighted_bce_loss

    if net.gumbel_softmax:
        gen_loss = disc_gen_loss(validity, torch.ones_like(validity).type(torch.cuda.FloatTensor), weights.to(device).unsqueeze(-1))
    else:
        gen_loss = disc_gen_loss(validity, torch.ones_like(validity).type(torch.cuda.FloatTensor), weights.to(device).unsqueeze(-1) * (log_prob).sum(-1).unsqueeze(-1))

    if add_fakes and not disc_only:
        scores_t = get_random_score(batch['score'].to(device))
        weights_t = weight_fn(scores_t)
        output_t, log_prob_t = net(scores_t, img=batch['image'], with_logits=True)
        validity_t = net.discriminator_forward(scores_t, output_t, img=batch['image'])

        if net.gumbel_softmax:
            gen_loss_t = disc_gen_loss(validity_t, torch.ones_like(validity_t).type(torch.cuda.FloatTensor), weights_t.to(device).unsqueeze(-1))
        else:
            gen_loss_t = disc_gen_loss(validity_t, torch.ones_like(validity_t).type(torch.cuda.FloatTensor), weights_t.to(device).unsqueeze(-1) * (log_prob_t).sum(-1).unsqueeze(-1))

        gen_loss = (gen_loss + gen_loss_t)*0.5

    net.zero_grad()
    gen_loss.backward()
    optimizer_g.step()

    output_gen, _ = net(batch['score'].to(device), img=batch['image'], with_logits=True, sample_hard=True)
    validity_real = net.discriminator_forward(batch['score'].to(device), batch['location'].type(torch.cuda.FloatTensor), img=batch['image']) #.to(device))
    d_real_loss = disc_gen_loss(validity_real, torch.ones_like(validity_real).type(torch.cuda.FloatTensor), weights.unsqueeze(-1))
    validity_fake = net.discriminator_forward(batch['score'], output_gen.detach(), img=batch['image'])
    d_fake_loss = disc_gen_loss(validity_fake, torch.zeros_like(validity_fake).type(torch.cuda.FloatTensor), weights.unsqueeze(-1))
    d_loss = (d_real_loss + d_fake_loss)/2
    disc_fake_acc = (torch.sigmoid(validity_fake) < 0.5).type(torch.cuda.FloatTensor).mean()
    disc_real_acc = (torch.sigmoid(validity_real) > 0.5).type(torch.cuda.FloatTensor).mean()

    if add_fakes:
        scores_t = get_random_score(batch['score'].to(device))
        weights_t = weight_fn(scores_t)
        output_gen_t, _ = net(scores_t, img=batch['image'], with_logits=True, sample_hard=True)
        validity_fake_t = net.discriminator_forward(scores_t, output_gen_t.detach(), img=batch['image'])
        d_fake_loss_t = disc_gen_loss(validity_fake_t, torch.zeros_like(validity_fake_t).type(torch.cuda.FloatTensor), weights_t.unsqueeze(-1))
        d_loss = (d_real_loss + (d_fake_loss + d_fake_loss_t)*0.5)*0.5

    net.zero_grad()
    d_loss.backward()
    optimizer_d.step()

    return gen_loss, d_loss, disc_fake_acc, disc_real_acc

def train_forward_net_with_weighting(net, optimizer, batch, weights):
    # import ipdb; ipdb.set_trace()
    score = batch['score']
    img = batch['image']
    location = batch['location']
    location_index = torch.argmax(location, dim=-1)
    score_discrete = score.type(torch.cuda.LongTensor)

    predictions = net(score=location.type(torch.cuda.FloatTensor), cond=img)
    # predictions = net(score=location.type(torch.cuda.FloatTensor), x=img)
    # softmax distribution in here
    # import ipdb; ipdb.set_trace()
    xent_loss = torch.nn.CrossEntropyLoss(reduction='none')
    loss = xent_loss(predictions, score_discrete.squeeze(-1))
    
    final_loss = (weights * loss).mean()

    net.zero_grad()
    final_loss.backward()
    optimizer.step()

    return final_loss
    
