import argparse
import os
from os import path
import time
import copy
import torch
import numpy as np
from torch import nn
from gan_training import utils
from gan_training.train import Trainer, update_average
from gan_training.weighted_train import WeightedTrainer, ForwardTrainer
from gan_training.logger import Logger
from gan_training.checkpoints import CheckpointIO
from gan_training.inputs import get_dataset
from gan_training.distributions import get_ydist, get_zdist
from gan_training.eval import Evaluator, ForwardModelEvaluator
from gan_training.config import (
    load_config, build_models, build_optimizers, build_lr_scheduler, build_forward_model, build_forward_optimizers
)

# Arguments
parser = argparse.ArgumentParser(
    description='Train a GAN with different regularization strategies.'
)
parser.add_argument('config', type=str, help='Path to config file.')
parser.add_argument('--no-cuda', action='store_true', help='Do not use cuda.')
parser.add_argument('--gpu', type=int, default=-1)
parser.add_argument('--only_forward', action='store_true', help='Only forward model to be trained')
parser.add_argument('--eval_best_z', action='store_true', help='Evaluate the best z')

args = parser.parse_args()

# Hack for if not kitten
if not os.path.exists('/data1/vgan_imdb'):
    os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu)

config = load_config(args.config)
is_cuda = (torch.cuda.is_available() and not args.no_cuda)

# Set seed:
if is_cuda:
    torch.backends.cudnn.deterministic = True
torch.manual_seed(0)

# Short hands
batch_size = config['training']['batch_size']
d_steps = config['training']['d_steps']
restart_every = config['training']['restart_every']
inception_every = config['training']['inception_every']
compute_fid = config['training']['compute_fid']
fid_sample_size = config['training']['fid_sample_size']
save_every = config['training']['save_every']
backup_every = config['training']['backup_every']
stop_epoch = config['training']['stop_epoch']
sample_nlabels = config['training']['sample_nlabels']
weighting_scheme = config['training']['weighting_scheme']

out_dir = config['training']['out_dir']
checkpoint_dir = path.join(out_dir, 'chkpts')

adaptive_beta = config['training']['adaptive_beta']

# Create missing directories
if not path.exists(out_dir):
    os.makedirs(out_dir)
if not path.exists(checkpoint_dir):
    os.makedirs(checkpoint_dir)

# Logger
checkpoint_io = CheckpointIO(
    checkpoint_dir=checkpoint_dir
)

device = torch.device("cuda:0" if is_cuda else "cpu")

# import ipdb; ipdb.set_trace()
# Dataset
train_dataset, nlabels = get_dataset(
    name=config['data']['type'],
    data_dir=config['data']['train_dir'],
    size=config['data']['img_size'],
    lsun_categories=config['data']['lsun_categories_train'],
    continuous=config['data']['continuous'],
    cropped_above=config['data']['cropped_above'],
    cropping_score=config['data']['cropping_score']
)
train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=batch_size,
        num_workers=config['training']['nworkers'],
        shuffle=True, pin_memory=True, sampler=None, drop_last=True
)

# import ipdb; ipdb.set_trace()
# for x, y in train_loader:
#     print (x, y)

# Number of labels
nlabels = min(nlabels, config['data']['nlabels'])
sample_nlabels = min(nlabels, sample_nlabels)

# Create models
generator, discriminator = build_models(config)
forward_model = build_forward_model(config)
print(generator)
print(discriminator)
print (forward_model)

# Put models on gpu if needed
generator = generator.to(device)
discriminator = discriminator.to(device)
forward_model = forward_model.to(device)

g_optimizer, d_optimizer = build_optimizers(
    generator, discriminator, config
)

f_optimizer = build_forward_optimizers(forward_model, config)

# Use multiple GPUs if possible
generator = nn.DataParallel(generator)
discriminator = nn.DataParallel(discriminator)
forward_model = nn.DataParallel(forward_model)

# Register modules to checkpoint
if not args.only_forward:
    checkpoint_io.register_modules(
        generator=generator,
        discriminator=discriminator,
        forward_model=forward_model,
        g_optimizer=g_optimizer,
        d_optimizer=d_optimizer,
        f_optimizer=f_optimizer,
    )
else:
    checkpoint_io.register_modules(
        forward_model=forward_model,
        f_optimizer=f_optimizer,
    )

# Logger
logger = Logger(
    log_dir=path.join(out_dir, 'logs'),
    img_dir=path.join(out_dir, 'imgs'),
    monitoring=config['training']['monitoring'],
    monitoring_dir=path.join(out_dir, 'monitoring')
)

# Distributions
ydist = get_ydist(nlabels, device=device)
zdist = get_zdist(config['z_dist']['type'], config['z_dist']['dim'],
                  device=device)

# Save for tests
ntest = batch_size
x_real, ytest = utils.get_nsamples(train_loader, ntest)
ytest.clamp_(None, nlabels-1)
ytest.squeeze(-1)
ztest = zdist.sample((ntest,))
utils.save_images(x_real, path.join(out_dir, 'real.png'))

# Test generator
if config['training']['take_model_average']:
    generator_test = copy.deepcopy(generator)
    checkpoint_io.register_modules(generator_test=generator_test)
else:
    generator_test = generator

# # Evaluator
evaluator = Evaluator(generator_test, zdist, ydist,
                        batch_size=batch_size, device=device)
forward_evaluator = ForwardModelEvaluator(
    generator=generator,
    forward_model=forward_model,
    zdist=zdist, ydist=ydist, device=device,
    lamda=config['eval']['lamda'], nu=config['eval']['nu'],
    num_itrs=config['eval']['num_itrs'],
    z_dim=config['z_dist']['dim'],
    maximize=config['eval']['maximize'],
)

# Train
tstart = t0 = time.time()
it = epoch_idx = -1

# Load checkpoint if existant
it = checkpoint_io.load('model.pt')
if it != -1:
    logger.load_stats('stats.p')
    if adaptive_beta:
        # Set reg_param to the last reg_param
        reg_param = logger.stats['learning_rates']['beta_value'][-1]
        reg_param = reg_param[1].item()
        print('Loading regparam to %.2f (default: %.2f)' % (config['training']['reg_param'], reg_param))
        config['training']['reg_param'] = reg_param

# Reinitialize model average if needed
if (config['training']['take_model_average']
        and config['training']['model_average_reinit']):
    update_average(generator_test, generator, 0.)

# Learning rate anneling
g_scheduler = build_lr_scheduler(g_optimizer, config, last_epoch=it)
d_scheduler = build_lr_scheduler(d_optimizer, config, last_epoch=it)
f_scheduler = build_lr_scheduler(f_optimizer, config, last_epoch=it)     # exactly the same as other two schedulers

# Trainer
if weighting_scheme is None or weighting_scheme == 'none':
    trainer = Trainer(
        generator, discriminator, g_optimizer, d_optimizer,
        gan_type=config['training']['gan_type'],
        reg_type=config['training']['reg_type'],
        reg_param=config['training']['reg_param'],
        adaptive_beta=adaptive_beta,
        add_fakes=config['training']['add_fakes'],
        **config['training']['kwargs']
    )
else:
    trainer = WeightedTrainer(
        train_dataset,
        generator, discriminator, g_optimizer, d_optimizer,
        weighting_scheme, nlabels=nlabels,
        gan_type=config['training']['gan_type'],
        reg_type=config['training']['reg_type'],
        reg_param=config['training']['reg_param'],
        adaptive_beta=adaptive_beta,
        add_fakes=config['training']['add_fakes'],
        continuous=config['training']['continuous'],
        **config['training']['kwargs']
    )

forward_trainer = ForwardTrainer(
    forward_model, f_optimizer, continuous=config['training']['continuous'], nlabels=nlabels, 
    train_dataset=train_dataset, weighting_scheme=weighting_scheme
)

# Training loop
print('Start training...')
while True:
    epoch_idx += 1
    print('Start epoch %d...' % epoch_idx)

    for x_real, y in train_loader:
        # import ipdb; ipdb.set_trace()
        it += 1

        g_scheduler.step()
        d_scheduler.step()
        f_scheduler.step()

        d_lr = d_optimizer.param_groups[0]['lr']
        g_lr = g_optimizer.param_groups[0]['lr']
        f_lr = f_optimizer.param_groups[0]['lr']
        logger.add('learning_rates', 'discriminator', d_lr, it=it)
        logger.add('learning_rates', 'generator', g_lr, it=it)
        logger.add('learning_rates', 'forward_model', f_lr, it=it)

        x_real, y = x_real.to(device), y.to(device)
        y = y.squeeze(-1)
        if not config['data']['continuous']:
            y.clamp_(None, nlabels-1)

        # Forward model updates
        floss = forward_trainer.forward_trainstep(x_real, y)
        logger.add('losses', 'forward_model', floss, it=it)
        if args.only_forward:
            f_loss_last = logger.get_last('losses', 'forward_model')
            print ('[epoch %0d, it %4d] f_loss = %.4f'
              % (epoch_idx, it, f_loss_last))
            
            # (iv) Save checkpoint if necessary
            if time.time() - t0 > save_every:
                print('Saving checkpoint...')
                checkpoint_io.save(it, 'model.pt')
                logger.save_stats('stats.p')
                t0 = time.time()

                if (restart_every > 0 and t0 - tstart > restart_every):
                    exit(3)
            continue

        # Discriminator updates
        z = zdist.sample((batch_size,))
        dloss, reg, accuracies = trainer.discriminator_trainstep(x_real, y, z)
        logger.add('losses', 'discriminator', dloss, it=it)
        logger.add('losses', 'regularizer', reg, it=it)
        logger.add('acc', 'real', accuracies['real'], it=it)
        logger.add('acc', 'fake', accuracies['fake'], it=it)

        if adaptive_beta:
            logger.add('learning_rates', 'beta_value', trainer.reg_param, it=it)

        # Generators updates
        if ((it + 1) % d_steps) == 0:
            z = zdist.sample((batch_size,))
            gloss = trainer.generator_trainstep(y, z)
            logger.add('losses', 'generator', gloss, it=it)

            if config['training']['take_model_average']:
                update_average(generator_test, generator,
                               beta=config['training']['model_average_beta'])

        # Print stats
        g_loss_last = logger.get_last('losses', 'generator')
        d_loss_last = logger.get_last('losses', 'discriminator')
        d_reg_last = logger.get_last('losses', 'regularizer')
        f_loss_last = logger.get_last('losses', 'forward_model')
        print('[epoch %0d, it %4d] g_loss = %.4f, d_loss = %.4f, reg=%.4f, f_loss=%.4f'
              % (epoch_idx, it, g_loss_last, d_loss_last, d_reg_last, f_loss_last))

        # (i) Sample if necessary
        if (it % config['training']['sample_every']) == 0:
            print('Creating samples...')
            ytest = ytest.reshape(ytest.shape[0])
            x = evaluator.create_samples(ztest, ytest)
            logger.add_imgs(x, 'all', it)
            for y_inst in range(sample_nlabels):
                x = evaluator.create_samples(ztest, y_inst)
                logger.add_imgs(x, '%04d' % y_inst, it)
        
        # Perform extensive sampling
        if (it % config['training']['sample_every']) == 0 and args.eval_best_z:
            print ('Creating optimized samples....')
            ytemp = torch.tensor(np.array([1.5,])).type(torch.cuda.FloatTensor)
            x, single_max, all_samples = forward_evaluator.create_samples(ystart=ytemp)
            # import ipdb; ipdb.set_trace()
            logger.add_imgs(x, 'optimized', it)
            logger.add_imgs(single_max, 'single_max_optimized', it)
            for jdx, sample in enumerate(all_samples):
                logger.add_imgs(sample, 'trend_of_optimization_' + str(jdx), it)
        # (ii) Compute inception if necessary: Don't compute this
        # if inception_every > 0 and ((it + 1) % inception_every) == 0:
        #     print('Computing inception/fid!')
        #     t0 = time.time()
        #     inception_mean, inception_std, fid = evaluator.compute_inception_score()
        #     t1 = time.time()
        #     print('took %.2f seconds' % (t1-t0))
        #     logger.add('inception_score', 'mean', inception_mean, it=it)
        #     logger.add('inception_score', 'stddev', inception_std, it=it)
        #     logger.add('fid', 'mean', fid, it=it)
        #     print('test it %d: IS: mean %.2f, std %.2f, FID: mean %.2f' % (it, inception_mean, inception_std, fid))

        # (iii) Backup if necessary
        if ((it + 1) % backup_every) == 0:
            print('Saving backup...')
            checkpoint_io.save(it, 'model_%08d.pt' % it)
            logger.save_stats('stats_%08d.p' % it)

        # (iv) Save checkpoint if necessary
        if time.time() - t0 > save_every or (it + 1) % 1000 == 0:
            print('Saving checkpoint...')
            checkpoint_io.save(it, 'model.pt')
            logger.save_stats('stats.p')
            t0 = time.time()
    
    print('Saving checkpoint...')
    checkpoint_io.save(it, 'model.pt')
    logger.save_stats('stats.p')
            
