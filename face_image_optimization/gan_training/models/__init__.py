from gan_training.models import (
    resnet, resnet_forward
)

generator_dict = {
    'resnet': resnet.Generator,
}

discriminator_dict = {
    'resnet': resnet.Discriminator,
}

forward_dict = {
    'resnet18': resnet_forward.resnet18,
    'resnet34': resnet_forward.resnet34,
}