# coding: utf-8
import torch
from torch.nn import functional as F
import torch.utils.data
import torch.utils.data.distributed
from torch import autograd
import numpy as np

def softmax(arr, temp=1.0):
    max_arr = arr.max()
    arr_new = arr - max_arr
    exp_arr = np.exp(arr_new/temp)
    return exp_arr / np.sum(exp_arr)

def logsoftmax(arr, temp=1.0):
    return np.log(softmax(arr, temp=temp) + 1e-7)

def softmin(arr, temp=1.0):
    return softmax(-arr, temp=temp)

def weighted_softmax(arr, weights, temp=1.0):
    arr_new = arr + np.log(weights + 1e-7)
    return softmax(arr_new, temp=temp)

def adaptive_temp(arr, temp=None):
    if arr.shape[0] < 10:
        return temp
    max_arr = arr.max()
    arr_new = arr - max_arr
    quantile_ninety = np.quantile(arr_new, q=0.9)
    return np.abs(quantile_ninety)

class ForwardTrainer(object):
    def __init__(self, forward_net, forward_optimizer, reg_type=None, reg_param=None, nlabels=10, continuous=True, weighting_scheme='biased_softmin', train_dataset=None, **kwargs):
        self.forward_net = forward_net
        self.forward_optimizer = forward_optimizer
        self.nlabels = nlabels

        self.continuous = continuous
        self.dataset = train_dataset
        self.weighting_scheme = weighting_scheme
        self.compute_weights()
        
    def compute_weights(self, ):
        all_scores = []
        nlabels = self.nlabels

        # import ipdb; ipdb.set_trace()
        # Compute all forms of weighting here
        # all_scores is already binned
        # hist_counts = np.array([np.count_nonzero(self.all_scores_np == x) for x in range(nlabels)])
        # self.hist_counts = hist_counts     # counts of all bins
        # hist_prob = self.hist_counts/np.sum(self.hist_counts)

        hist_counts = self.dataset.freq_count
        self.hist_counts = hist_counts
        hist_prob = self.hist_counts / np.sum(self.hist_counts)

        if self.weighting_scheme == 'none' or self.weighting_scheme is None:
            self.weights = np.array([1.0,] * len(self.hist_counts))
            self.weights_t = torch.Tensor(self.weights).type(torch.cuda.FloatTensor)
        elif self.weighting_scheme == 'biased_softmax':
            """Softmax weighting scheme"""
            # import ipdb; ipdb.set_trace()
            # bin_scores = np.arange(nlabels) * 10.0 + np.abs(np.random.normal(loc=0.0, scale=2.0, size=(nlabels, )))
            bin_scores = np.arange(0, 100, int(100//nlabels))
            base_temp = 40.0
            base_temp = adaptive_temp(bin_scores, temp=base_temp)
            softmax_prob = softmax(bin_scores, temp=base_temp)
            softmax_reweight = softmax_prob *  hist_prob/ (hist_prob + 1e-2)
            softmax_reweight = softmax_reweight / np.sum(softmax_reweight) 
            self.weights = softmax_reweight / (hist_prob + 1e-6)
            self.weights_t = torch.Tensor(self.weights).type(torch.cuda.FloatTensor)
        elif self.weighting_scheme == 'biased_softmin':
            # bin_scores = np.arange(nlabels) * 10.0 + np.abs(np.random.normal(loc=0.0, scale=2.0, size=(nlabels, )))
            ### WEIGHTING SCHEME USED IN MODEL INVERSION NETWORKS FOR AGE PREDICTION
            ## set a base temperature, and adapt the temperature based on the current batch of scores
            ## For reweighting, perform reweighting based on N_y / (N_y + K) * softmax(score/temperature)
            bin_scores = np.arange(0, 100, int(100//nlabels))
            base_temp = 40.0
            # Adaptively deciding the temperature
            base_temp = adaptive_temp(-bin_scores, temp=base_temp)
            # Computing the soft minimum (since socre is age here, and we need to minimize the age)
            softmax_prob = softmin(bin_scores, temp=base_temp)
            # Performing reweighting on N_y / (N_y + K), N_y = hist_prob, K = 1e-2, the softmax_prob is the score
            softmax_reweight = softmax_prob *  hist_prob/ (hist_prob + 1e-2)
            # Making the weights a normalized distribution
            softmax_reweight = softmax_reweight / np.sum(softmax_reweight)
            # Importance sampling for reweighting
            self.weights = softmax_reweight / (hist_prob + 1e-6)
            self.weights_t = torch.Tensor(self.weights).type(torch.cuda.FloatTensor)
        elif self.weighting_scheme == 'provable':
            """Provably optimal weighting scheme"""
            bin_scores = np.arange(nlabels) * 10.0 + np.abs(np.random.normal(loc=0.0, scale=2.0, size=(nlabels, )))
            logits_new = bin_scores + 40.0 * np.log(np.sqrt(hist_prob + 1e-8))
            softmax_prob = softmax(logits_new, temp=40.0)
            self.weights = softmax_prob / (hist_prob + 1e-6) * (hist_prob / (hist_prob + 1e-3))
            self.weights_t = torch.Tensor(self.weights).type(torch.cuda.FloatTensor)
        else:
            raise NotImplementedError
        
        self.weights_t = torch.clamp(self.weights_t, min=0.0, max=6.0)
        print ('Weights: ', self.weights_t)
        # import ipdb; ipdb.set_trace()
    
    def forward_trainstep(self, x, y):
        assert (y.size(0) == x.size(0))
        self.forward_net.train()
        self.forward_optimizer.zero_grad()

        y = y.type(torch.cuda.FloatTensor)
        if self.continuous:
            weights = self.weights_t[y.type(torch.cuda.LongTensor)]
        else:
            weights = self.weights_t[y]

        forward_out = self.forward_net(x)
        floss = (forward_out - y)**2
        floss = (floss.squeeze(-1) * weights).mean()

        floss.backward()
        self.forward_optimizer.step()

        return floss.item()


class WeightedTrainer(object):
    def __init__(self, dataset, generator, discriminator, g_optimizer, d_optimizer, weighting_scheme, nlabels,
                 gan_type, reg_type, reg_param, adaptive_beta, add_fakes=False, continuous=False, **kwargs):
        self.generator = generator
        self.discriminator = discriminator
        self.g_optimizer = g_optimizer
        self.d_optimizer = d_optimizer
        self.dataset = dataset
        self.weighting_scheme = weighting_scheme
        self.nlabels = nlabels

        self.gan_type = gan_type
        self.reg_type = reg_type
        self.reg_param = reg_param
        self.add_fakes = add_fakes
        self.continuous = continuous

        self.adaptive_beta = adaptive_beta
        if self.adaptive_beta:
            self.target_kl = kwargs['target_kl']
            self.beta_step = kwargs['beta_step']
        
        self.compute_weights()

    def compute_weights(self, ):
        all_scores = []
        nlabels = self.nlabels

        # import ipdb; ipdb.set_trace()
        # Compute all forms of weighting here
        # all_scores is already binned
        # hist_counts = np.array([np.count_nonzero(self.all_scores_np == x) for x in range(nlabels)])
        # self.hist_counts = hist_counts     # counts of all bins
        # hist_prob = self.hist_counts/np.sum(self.hist_counts)

        hist_counts = self.dataset.freq_count
        self.hist_counts = hist_counts
        hist_prob = self.hist_counts / np.sum(self.hist_counts)

        if self.weighting_scheme == 'none' or self.weighting_scheme is None:
            self.weights = np.array([1.0,] * len(self.hist_counts))
            self.weights_t = torch.Tensor(self.weights).type(torch.cuda.FloatTensor)
        elif self.weighting_scheme == 'biased_softmax':
            """Softmax weighting scheme"""
            # import ipdb; ipdb.set_trace()
            # bin_scores = np.arange(nlabels) * 10.0 + np.abs(np.random.normal(loc=0.0, scale=2.0, size=(nlabels, )))
            bin_scores = np.arange(0, 100, int(100//nlabels))
            base_temp = 40.0
            # bin_scores = np.arange(nlabels) * 10.0 + np.abs(np.random)
            base_temp = adaptive_temp(bin_scores, temp=base_temp)
            softmax_prob = softmax(bin_scores, temp=base_temp)
            softmax_reweight = softmax_prob *  hist_prob/ (hist_prob + 1e-2)
            softmax_reweight = softmax_reweight / np.sum(softmax_reweight) 
            self.weights = softmax_reweight / (hist_prob + 1e-6)
            self.weights_t = torch.Tensor(self.weights).type(torch.cuda.FloatTensor)
        elif self.weighting_scheme == 'biased_softmin':
            # bin_scores = np.arange(nlabels) * 10.0 + np.abs(np.random.normal(loc=0.0, scale=2.0, size=(nlabels, )))
            bin_scores = np.arange(0, 100, int(100//nlabels))
            base_temp = 40.0
            base_temp = adaptive_temp(-bin_scores, temp=base_temp)
            softmax_prob = softmin(bin_scores, temp=base_temp)
            softmax_reweight = softmax_prob *  hist_prob/ (hist_prob + 1e-2)
            softmax_reweight = softmax_reweight / np.sum(softmax_reweight) 
            self.weights = softmax_reweight / (hist_prob + 1e-6)
            self.weights_t = torch.Tensor(self.weights).type(torch.cuda.FloatTensor)
        elif self.weighting_scheme == 'provable':
            """Provably optimal weighting scheme"""
            bin_scores = np.arange(nlabels) * 10.0 + np.abs(np.random.normal(loc=0.0, scale=2.0, size=(nlabels, )))
            logits_new = bin_scores + 40.0 * np.log(np.sqrt(hist_prob + 1e-8))
            softmax_prob = softmax(logits_new, temp=40.0)
            self.weights = softmax_prob / (hist_prob + 1e-6) * (hist_prob / (hist_prob + 1e-6))
            self.weights_t = torch.Tensor(self.weights).type(torch.cuda.FloatTensor)
        else:
            raise NotImplementedError
        
        self.weights_t = torch.clamp(self.weights_t, min=0.0, max=6.0)
        print ('Weights: ', self.weights_t)
        # import ipdb; ipdb.set_trace()

    def generator_trainstep(self, y, z):
        assert(y.size(0) == z.size(0))
        # import ipdb; ipdb.set_trace()
        toogle_grad(self.generator, True)
        toogle_grad(self.discriminator, False)
        self.generator.train()
        self.discriminator.train()
        self.g_optimizer.zero_grad()

        x_fake = self.generator(z, y)
        d_fake = self.discriminator(x_fake, y)
        d_fake = d_fake['out']
        if self.continuous:
            weights = self.weights_t[y.type(torch.cuda.LongTensor)]
        else:
            weights = self.weights_t[y]
        gloss = self.compute_loss(d_fake, 1, weights=weights)

        if self.add_fakes:
            y_t = (torch.rand(*y.shape)*10).type(torch.cuda.LongTensor)
            x_fake_t = self.generator(z, y_t)
            if self.continuous:
                weights = self.weights_t[y_t.type(torch.cuda.LongTensor)]
            else:
                weights = self.weights_t[y_t]
            d_fake_dict_t = self.discriminator(x_fake_t, y_t)
            d_fake_t = d_fake_dict_t['out']
            dloss_fake_t = self.compute_loss(d_fake_t, 1, weights=weights)
            gloss = (dloss_fake_t + gloss)*0.5

        gloss.backward()

        self.g_optimizer.step()

        return gloss.item()

    def discriminator_trainstep(self, x_real, y, z):
        toogle_grad(self.generator, False)
        toogle_grad(self.discriminator, True)
        self.generator.train()
        self.discriminator.train()
        self.d_optimizer.zero_grad()

        # On real data
        x_real.requires_grad_()

        # import ipdb; ipdb.set_trace()
        d_real_dict = self.discriminator(x_real, y)
        d_real = d_real_dict['out']
        if self.continuous:
            weights_real = self.weights_t[y.type(torch.cuda.LongTensor)]
        else:
            weights_real = self.weights_t[y]
        dloss_real = self.compute_loss(d_real, 1, weights=weights_real)

        reg = 0.

        if self.reg_type == 'real' or self.reg_type == 'instnoise_real':
            dloss_real.backward(retain_graph=True)
            reg = self.reg_param * compute_grad2(d_real, x_real).mean()
            reg.backward()
        elif self.reg_type == 'vgan':
            dloss_real.backward(retain_graph=True)
            mu = d_real_dict['mu']
            logstd = d_real_dict['logstd']
            kl_real = kl_loss(mu, logstd).mean()
        elif self.reg_type == 'vgan_real':
            # Both grad penal and vgan!
            dloss_real.backward(retain_graph=True)
            # TODO: rm hard coded 10 weight for grad penal.
            reg += 10. * compute_grad2(d_real, x_real).mean()
            mu = d_real_dict['mu']
            logstd = d_real_dict['logstd']
            kl_real = kl_loss(mu, logstd).mean()
        else:
            # No reguralization.
            dloss_real.backward()

        d_acc_real = torch.mean((d_real > 0.5).float())

        # On fake data
        with torch.no_grad():
            x_fake = self.generator(z, y)

        x_fake.requires_grad_()
        d_fake_dict = self.discriminator(x_fake, y)
        if self.continuous:
            weights_fake = self.weights_t[y.type(torch.cuda.LongTensor)]
        else:
            weights_fake = self.weights_t[y]
        d_fake = d_fake_dict['out']
        dloss_fake = self.compute_loss(d_fake, 0, weights_fake)

        if self.add_fakes:
            y_t = (torch.rand(*y.shape)*10).type(torch.cuda.LongTensor)
            with torch.no_grad():
                x_fake_t = self.generator(z, y_t)

            x_fake_t.requires_grad_()
            if self.continuous:
                weights_t = self.weights_t[y_t.type(torch.cuda.LongTensor)]
            else:
                weights_t = self.weights_t[y_t]
            d_fake_dict_t = self.discriminator(x_fake_t, y_t)
            d_fake_t = d_fake_dict_t['out']
            dloss_fake_t = self.compute_loss(d_fake_t, 0, weights=weights_t)

            dloss_fake = (dloss_fake_t + dloss_fake)*0.5
            
        if self.reg_type == 'fake':
            dloss_fake.backward(retain_graph=True)
            reg = self.reg_param * compute_grad2(d_fake, x_fake).mean()
            reg.backward()
        elif self.reg_type == 'vgan' or self.reg_type == 'vgan_real':
            dloss_fake.backward(retain_graph=True)
            mu_fake = d_fake_dict['mu']
            logstd_fake = d_fake_dict['logstd']
            kl_fake = kl_loss(mu_fake, logstd_fake).mean()

            if self.add_fakes:
                mu_fake_t = d_fake_dict_t['mu']
                logstd_fake_t = d_fake_dict_t['logstd']
                kl_fake_t = kl_loss(mu_fake_t, logstd_fake_t).mean()
                kl_fake = (kl_fake + kl_fake_t) * 0.5

            avg_kl = 0.5 * (kl_real + kl_fake)
            reg += self.reg_param * avg_kl
            reg.backward()
        else:
            dloss_fake.backward()

        d_acc_fake = torch.mean((d_fake < 0.5).float())
        accuracies = {'real': d_acc_real, 'fake': d_acc_fake}

        if self.reg_type == 'wgangp':
            reg = self.reg_param * self.wgan_gp_reg(x_real, x_fake, y)
            reg.backward()

        if self.adaptive_beta:
            self.update_beta(avg_kl)

        self.d_optimizer.step()

        toogle_grad(self.discriminator, False)

        # Output
        dloss = (dloss_real + dloss_fake)

        if self.reg_type == 'none' or self.reg_type == 'instnoise':
            reg = torch.tensor(0.)

        # hack to fix div by zero
        clamp_reg_param = max(self.reg_param, 1e-5)
        reg_raw = reg / clamp_reg_param

        return dloss.item(), reg_raw.item(), accuracies

    def compute_loss(self, d_out, target, weights):
        targets = d_out.new_full(size=d_out.size(), fill_value=target)

        if self.gan_type == 'standard':
            loss = torch.nn.BCEWithLogitsLoss(reduce=False)
            loss_val = loss(d_out, targets) * weights
            loss = loss_val.mean()
            # loss = F.binary_cross_entropy_with_logits(d_out, targets)
        elif self.gan_type == 'wgan':
            loss = (2*target - 1) * d_out.mean()
        else:
            raise NotImplementedError

        return loss

    def wgan_gp_reg(self, x_real, x_fake, y):
        batch_size = y.size(0)
        eps = torch.rand(batch_size, device=y.device).view(batch_size, 1, 1, 1)
        x_interp = (1 - eps) * x_real + eps * x_fake
        x_interp = x_interp.detach()
        x_interp.requires_grad_()
        d_out = self.discriminator(x_interp, y)
        d_out = d_out['out']

        reg = (compute_grad2(d_out, x_interp).sqrt() - 1.).pow(2).mean()

        return reg

    def update_beta(self, avg_kl):
        with torch.no_grad():
            new_beta = self.reg_param - self.beta_step * (self.target_kl - avg_kl)
            new_beta = max(new_beta, 0)
            # print('setting beta from %.2f to %.2f' % (self.reg_param, new_beta))
            self.reg_param = new_beta


# Utility functions
def toogle_grad(model, requires_grad):
    for p in model.parameters():
        p.requires_grad_(requires_grad)


def compute_grad2(d_out, x_in):
    batch_size = x_in.size(0)
    grad_dout = autograd.grad(
        outputs=d_out.sum(), inputs=x_in,
        create_graph=True, retain_graph=True, only_inputs=True
    )[0]
    grad_dout2 = grad_dout.pow(2)
    assert(grad_dout2.size() == x_in.size())
    reg = grad_dout2.view(batch_size, -1).sum(1)
    return reg


def kl_loss(mu, logstd):
    # mu and logstd are b x k x d x d
    # make them into b*d*d x k

    dim = mu.shape[1]
    mu = mu.permute(0, 2, 3, 1).contiguous()
    logstd = logstd.permute(0, 2, 3, 1).contiguous()
    mu = mu.view(-1, dim)
    logstd = logstd.view(-1, dim)

    std = torch.exp(logstd)
    kl = torch.sum(-logstd + 0.5 * (std**2 + mu**2), dim=-1) - (0.5 * dim)

    return kl

def update_average(model_tgt, model_src, beta):
    toogle_grad(model_src, False)
    toogle_grad(model_tgt, False)

    param_dict_src = dict(model_src.named_parameters())

    for p_name, p_tgt in model_tgt.named_parameters():
        p_src = param_dict_src[p_name]
        assert(p_src is not p_tgt)
        p_tgt.copy_(beta*p_tgt + (1. - beta)*p_src)
