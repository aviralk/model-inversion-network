import torch
import torchvision.transforms as transforms
import torchvision.datasets as datasets
from torch.utils.data import Dataset
from torchvision.datasets.folder import pil_loader, accimage_loader, default_loader
import numpy as np

from skimage import io
from torchvision import transforms

import glob, os
import re

age_cls_unit = 60
age_divide = 10

imdb_distr = [25, 63, 145, 54, 46, 113, 168, 232, 455, 556,
                752, 1089, 1285, 1654, 1819, 1844, 2334, 2828,
                3346, 4493, 6279, 7414, 7706, 9300, 9512, 11489,
                10481, 12483, 11280, 13096, 12766, 14346, 13296,
                12525, 12568, 12278, 12694, 11115, 12089, 11994,
                9960, 9599, 9609, 8967, 7940, 8267, 7733, 6292,
                6235, 5596, 5129, 4864, 4466, 4278, 3515, 3644,
                3032, 2841, 2917, 2755, 2853, 2380, 2169, 2084,
                1763, 1671, 1556, 1343, 1320, 1121, 1196, 949,
                912, 710, 633, 581, 678, 532, 491, 428, 367,
                339, 298, 203, 258, 161, 136, 134, 121, 63, 63,
                82, 40, 37, 24, 16, 18, 11, 4, 9]

def get_dataset(name, data_dir, size=64, lsun_categories=None, continuous=False, cropped_above=True, cropping_score=40):
    transform = transforms.Compose([
        transforms.Resize(size),
        transforms.CenterCrop(size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        transforms.Lambda(lambda x: x + 1./128 * torch.rand(x.size())),
    ])

    if name == 'imdb_wiki':
        dataset = FaceDataset(data_dir, transforms_=transform, continuous=continuous, cropped_above=cropped_above, cropping_score=cropping_score)
        nlabels = dataset.num_labels
    elif name == 'fgvc_aircraft':
        dataset =  FGVCAircraft(data_dir, 'variant', split='train'+'val', transform=transform, download=True)
        nlabels = 21
    elif name == 'image':
        dataset = datasets.ImageFolder(data_dir, transform)
        nlabels = len(dataset.classes)
    elif name == 'npy':
        # Only support normalization for now
        dataset = datasets.DatasetFolder(data_dir, npy_loader, ['npy'])
        nlabels = len(dataset.classes)
    elif name == 'cifar10':
        dataset = datasets.CIFAR10(root=data_dir, train=True, download=True,
                                   transform=transform)
        nlabels = 10
    elif name == 'lsun':
        if lsun_categories is None:
            lsun_categories = 'train'
        dataset = datasets.LSUN(data_dir, lsun_categories, transform)
        nlabels = len(dataset.classes)
    elif name == 'lsun_class':
        dataset = datasets.LSUNClass(data_dir, transform,
                                     target_transform=(lambda t: 0))
        nlabels = 1
    else:
        raise NotImplemented

    return dataset, nlabels

class FaceDataset(Dataset):
    """Face Dataset

       Please refer to https://github.com/jingkuan/Age-Gender-Pred for getting a cleaned dataset.
    """
    def __init__(self, root, num_labels=20, transforms_=None, cropped_above=True, continuous=False, cropping_score=40):
        if root[-1] != '/':
            print("[WARNING] PARAM: datapath SHOULD END WITH '/'")
            root += '/'
        self.datapath = root
        self.transformer = transforms_
        self.pics = [f[len(root) : ] for f in glob.glob(root + "*.jpg")]
        self.age_divde = float(age_divide)
        self.age_cls_unit = int(age_cls_unit)

        self.age_cls = {x: self.GaussianProb(x) for x in range(1, self.age_cls_unit + 1)}
        self.age_cls_zeroone = {x: self.ZeroOneProb(x) for x in range(1, self.age_cls_unit + 1)}
        self.num_labels = num_labels
        self.cropped_above = cropped_above
        self.continuous = continuous

        self.all_age_count = imdb_distr
        print (len(self.all_age_count))
        self.freq_count = np.array(self.all_age_count)
        self.valid_score_arr = self.freq_count
        self.valid_mask  = np.arange(self.valid_score_arr.shape[0])
        # import ipdb; ipdb.set_trace()
        print ('DATASET prop: ', self.cropped_above, self.continuous, cropping_score)

        if self.cropped_above:
            self.valid_score_arr = (self.valid_mask < cropping_score) * self.valid_score_arr
        else:
            self.valid_score_arr = (self.valid_mask > cropping_score) * self.valid_score_arr

        self.freq_count = np.reshape(self.valid_score_arr, [20, 5])
        print (self.freq_count)
        self.freq_count = np.sum(self.freq_count, 1)

        self.cropping_score = cropping_score

    def __len__(self):
        return len(self.pics)

    def GaussianProb(self, true, var = 2.5):
        x = np.array(range(1, self.age_cls_unit + 1), dtype='float')
        probs = np.exp(-np.square(x - true) / (2 * var ** 2)) / (var * (2 * np.pi ** .5))
        return probs / probs.max()
    
    def ZeroOneProb(self, true):
        x = np.zeros(shape=(self.age_cls_unit, ))
        x[true - 1] = 1
        return x
    
    def __getitem__(self, idx):
        """
        get images and labels
        :param idx: image index 
        :return: image: transformed image, gender: torch.LongTensor, age: torch.FloatTensor
        """
        # read image and labels
        # idx %= 100000
        img_name = self.datapath + self.pics[idx]
        img = io.imread(img_name)
        if len(img.shape) == 2: # gray image
            img = np.repeat(img[:, :, np.newaxis], 3, axis=2)
        (age, gender) = re.findall(r"([^_]*)_([^_]*)_[^_]*.jpg", self.pics[idx])[0]
        age = max(1., min(float(age), float(self.age_cls_unit)))
        
        # print (age)
        
        if self.cropped_above and age > self.cropping_score:
            # print ('Going')
            return self.__getitem__((idx + 1)% len(self.pics))
        elif (not self.cropped_above) and age < self.cropping_score:
            # print ('Going 2')
            return self.__getitem__((idx + 1)% len(self.pics))

        # print (age)
        # if age <= 25:
        #     print (age)
        # preprcess images
        if self.transformer:
            img = transforms.ToPILImage()(img)
            image = self.transformer(img)
        else:
            image = torch.from_numpy(img)

        # preprocess labels
        gender = float(gender)
        gender = torch.from_numpy(np.array([gender], dtype='float'))
        gender = gender.type(torch.LongTensor)

        age_rgs_label = torch.from_numpy(np.array([age / self.age_divde], dtype='float'))
        age_rgs_label = age_rgs_label.type(torch.FloatTensor)

        age_cls_label = self.age_cls[int(age)]
        # age_cls_label = self.age_cls_zeroone[int(age)]

        age_cls_label = torch.from_numpy(np.array([age_cls_label], dtype='float'))
        age_cls_label = age_cls_label.type(torch.FloatTensor)

        # print (img_name, age_cls_label, age_rgs_label)
        # image of shape [256, 256]
        # gender of shape [,1] and value in {0, 1}
        # age of shape [,1] and value in [0 ~ 10)
        if self.continuous:
            return image, age_rgs_label
        return image, age_rgs_label.type(torch.LongTensor)

def make_dataset(dir, image_ids, targets):
    assert(len(image_ids) == len(targets))
    images = []
    dir = os.path.expanduser(dir)
    for i in range(len(image_ids)):
        item = (os.path.join(dir, 'data', 'images',
                             '%s.jpg' % image_ids[i]), targets[i])
        images.append(item)
    return images

def find_classes(classes_file):
    # read classes file, separating out image IDs and class names
    image_ids = []
    targets = []
    f = open(classes_file, 'r')
    for line in f:
        split_line = line.split(' ')
        image_ids.append(split_line[0])
        targets.append(' '.join(split_line[1:]))
    f.close()

    # index class names
    classes = np.unique(targets)
    class_to_idx = {classes[i]: i for i in range(len(classes))}
    targets = [class_to_idx[c] for c in targets]

    return (image_ids, targets, classes, class_to_idx)

class FGVCAircraft(Dataset):
    """`FGVC-Aircraft <http://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft>`_ Dataset.
    Args:
        root (string): Root directory path to dataset.
        class_type (string, optional): The level of FGVC-Aircraft fine-grain classification
            to label data with (i.e., ``variant``, ``family``, or ``manufacturer``).
        transform (callable, optional): A function/transform that takes in a PIL image
            and returns a transformed version. E.g. ``transforms.RandomCrop``
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
        loader (callable, optional): A function to load an image given its path.
        download (bool, optional): If true, downloads the dataset from the internet and
            puts it in the root directory. If dataset is already downloaded, it is not
            downloaded again.
    """
    url = 'http://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft/archives/fgvc-aircraft-2013b.tar.gz'
    class_types = ('variant', 'family', 'manufacturer')
    splits = ('train', 'val', 'trainval', 'test')

    def __init__(self, root, class_type='variant', split='train', transform=None,
                 target_transform=None, loader=default_loader, download=False, continuous=True):
        if split not in self.splits:
            raise ValueError('Split "{}" not found. Valid splits are: {}'.format(
                split, ', '.join(self.splits),
            ))
        if class_type not in self.class_types:
            raise ValueError('Class type "{}" not found. Valid class types are: {}'.format(
                class_type, ', '.join(self.class_types),
            ))
        self.root = os.path.expanduser(root)
        self.class_type = class_type
        self.split = split
        self.classes_file = os.path.join(self.root, 'data',
                                         'images_%s_%s.txt' % (self.class_type, self.split))

        if download:
            self.download()

        (image_ids, targets, classes, class_to_idx) = find_classes(self.classes_file)
        samples = make_dataset(self.root, image_ids, targets)

        self.transform = transform
        self.target_transform = target_transform
        self.loader = loader

        self.samples = samples
        self.classes = classes
        self.class_to_idx = class_to_idx
        self.continuous = continuous

        self.variant_scores_dict = dict()
        with open('/home//seating.txt', 'r') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip('\n')
                line = line.replace('[', '')
                line = line.replace(']', '')
                line = line.replace(',', '')
                line = line.split(' ')
                key = str(line[0])
                print (line)
                values = [int(line[x+1]) for x in range(len(line)-1)]
                print (key, values)
                if len(values) == 1:
                    self.variant_scores_dict[key] = values[0]
                elif len(values) == 2:
                    self.variant_scores_dict[key] = (values[0] + values[1])/2.0
                else:
                    self.variant_scores_dict[key] = np.random.choice(values)
                print (key, values, self.variant_scores_dict[key])
        
        self.index_score_dict = dict()
        self.index_cont_score_dict = dict()
        self.variant_cont_score_dict = dict()

        for key in self.variant_scores_dict:
            self.variant_cont_score_dict[key] = self.variant_scores_dict[key] / 35.0
            self.variant_scores_dict[key] = int (int(self.variant_scores_dict[key]) / 35)
            key_found = False
            for key_class in self.class_to_idx.keys():
                if key in key_class:
                    self.index_score_dict[self.class_to_idx[key_class]] = self.variant_scores_dict[key]
                    self.index_cont_score_dict[self.class_to_idx[key_class]] = self.variant_cont_score_dict[key]
                    print (key, ' --> ', key_class)
                    key_found = True
            if not key_found:
                print ('Not found: ', key)
        
        print ('Num Keys: ', len(self.index_score_dict.keys()))
        
        cnt_arr = []
        cont_cnt_arr = []
        for sample in samples:
            cnt_arr.append(self.index_score_dict[sample[1]])
            cont_cnt_arr.append(self.index_cont_score_dict[sample[1]])
        
        # import ipdb; ipdb.set_trace()
        print ('Number elements: ', len(cnt_arr))
        counts = np.bincount(np.array(cnt_arr))
        self.freq_count = counts
        print ('Freq Count: ', counts)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index
        Returns:
            tuple: (sample, target) where target is class_index of the target class.
        """

        path, target = self.samples[index]
        sample = self.loader(path)
        if self.transform is not None:
            sample = self.transform(sample)
        if self.target_transform is not None:
            target = self.target_transform(target)

        if self.continuous:
            return sample, self.index_cont_score_dict[target]

        return sample, self.index_score_dict[target]

    def __len__(self):
        return len(self.samples)

    def __repr__(self):
        fmt_str = 'Dataset ' + self.__class__.__name__ + '\n'
        fmt_str += '    Number of datapoints: {}\n'.format(self.__len__())
        fmt_str += '    Root Location: {}\n'.format(self.root)
        tmp = '    Transforms (if any): '
        fmt_str += '{0}{1}\n'.format(tmp, self.transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
        tmp = '    Target Transforms (if any): '
        fmt_str += '{0}{1}'.format(tmp, self.target_transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
        return fmt_str

    def _check_exists(self):
        return os.path.exists(os.path.join(self.root, 'data', 'images')) and \
            os.path.exists(self.classes_file)

    def download(self):
        """Download the FGVC-Aircraft data if it doesn't exist already."""
        from six.moves import urllib
        import tarfile

        if self._check_exists():
            return

        # prepare to download data to PARENT_DIR/fgvc-aircraft-2013.tar.gz
        print('Downloading %s ... (may take a few minutes)' % self.url)
        parent_dir = os.path.abspath(os.path.join(self.root, os.pardir))
        tar_name = self.url.rpartition('/')[-1]
        tar_path = os.path.join(parent_dir, tar_name)
        data = urllib.request.urlopen(self.url)

        # download .tar.gz file
        with open(tar_path, 'wb') as f:
            f.write(data.read())

        # extract .tar.gz to PARENT_DIR/fgvc-aircraft-2013b
        data_folder = tar_path.strip('.tar.gz')
        print('Extracting %s to %s ... (may take a few minutes)' % (tar_path, data_folder))
        tar = tarfile.open(tar_path)
        tar.extractall(parent_dir)

        # if necessary, rename data folder to self.root
        if not os.path.samefile(data_folder, self.root):
            print('Renaming %s to %s ...' % (data_folder, self.root))
            os.rename(data_folder, self.root)

        # delete .tar.gz file
        print('Deleting %s ...' % tar_path)
        os.remove(tar_path)

        print('Done!')


# class FGVCAircraft(Dataset):
#     """`FGVC-Aircraft <http://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft>`_ Dataset.
#     Args:
#         root (string): Root directory path to dataset.
#         class_type (string, optional): The level of FGVC-Aircraft fine-grain classification
#             to label data with (i.e., ``variant``, ``family``, or ``manufacturer``).
#         transform (callable, optional): A function/transform that takes in a PIL image
#             and returns a transformed version. E.g. ``transforms.RandomCrop``
#         target_transform (callable, optional): A function/transform that takes in the
#             target and transforms it.
#         loader (callable, optional): A function to load an image given its path.
#         download (bool, optional): If true, downloads the dataset from the internet and
#             puts it in the root directory. If dataset is already downloaded, it is not
#             downloaded again.
#     """
#     url = 'http://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft/archives/fgvc-aircraft-2013b.tar.gz'
#     class_types = ('variant', 'family', 'manufacturer')
#     splits = ('train', 'val', 'trainval', 'test')

#     def __init__(self, root, class_type='variant', split='train', transform=None,
#                  target_transform=None, loader=default_loader, download=False):
#         if split not in self.splits:
#             raise ValueError('Split "{}" not found. Valid splits are: {}'.format(
#                 split, ', '.join(self.splits),
#             ))
#         if class_type not in self.class_types:
#             raise ValueError('Class type "{}" not found. Valid class types are: {}'.format(
#                 class_type, ', '.join(self.class_types),
#             ))
#         self.root = os.path.expanduser(root)
#         self.class_type = class_type
#         self.split = split
#         self.classes_file = os.path.join(self.root, 'data',
#                                          'images_%s_%s.txt' % (self.class_type, self.split))

#         if download:
#             self.download()

#         (image_ids, targets, classes, class_to_idx) = find_classes(self.classes_file)
#         samples = make_dataset(self.root, image_ids, targets)

#         self.transform = transform
#         self.target_transform = target_transform
#         self.loader = loader

#         self.samples = samples
#         self.classes = classes
#         self.class_to_idx = class_to_idx

#         self.variant_scores_dict = dict()
#         with open('/home//seating.txt', 'r') as f:
#             lines = f.readlines()
#             for line in lines:
#                 line = line.strip('\n')
#                 line = line.replace('[', '')
#                 line = line.replace(']', '')
#                 line = line.replace(',', '')
#                 line = line.split(' ')
#                 key = str(line[0])
#                 print (line)
#                 values = [int(line[x+1]) for x in range(len(line)-1)]
#                 print (key, values)
#                 if len(values) == 1:
#                     self.variant_scores_dict[key] = values[0]
#                 elif len(values) == 2:
#                     self.variant_scores_dict[key] = (values[0] + values[1])/2.0
#                 else:
#                     self.variant_scores_dict[key] = np.random.choice(values)
#                 print (key, values, self.variant_scores_dict[key])
        
#         self.index_score_dict = dict()
#         for key in self.variant_scores_dict:
#             self.variant_scores_dict[key] = int (int(self.variant_scores_dict[key]) / 35)
#             key_found = False
#             for key_class in self.class_to_idx.keys():
#                 if key in key_class:
#                     self.index_score_dict[self.class_to_idx[key_class]] = self.variant_scores_dict[key]
#                     print (key, ' --> ', key_class)
#                     key_found = True
#             if not key_found:
#                 print ('Not found: ', key)
        
#         print ('Num Keys: ', len(self.index_score_dict.keys()))
        
#         cnt_arr = []
#         for sample in samples:
#             cnt_arr.append(self.index_score_dict[sample[1]])
        
#         # import ipdb; ipdb.set_trace()
#         print ('Number elements: ', len(cnt_arr))
#         counts = np.bincount(np.array(cnt_arr))
#         self.freq_count = counts
#         print ('Freq Count: ', counts)

#     def __getitem__(self, index):
#         """
#         Args:
#             index (int): Index
#         Returns:
#             tuple: (sample, target) where target is class_index of the target class.
#         """

#         path, target = self.samples[index]
#         sample = self.loader(path)
#         if self.transform is not None:
#             sample = self.transform(sample)
#         if self.target_transform is not None:
#             target = self.target_transform(target)

#         return sample, self.index_score_dict[target]

#     def __len__(self):
#         return len(self.samples)

#     def __repr__(self):
#         fmt_str = 'Dataset ' + self.__class__.__name__ + '\n'
#         fmt_str += '    Number of datapoints: {}\n'.format(self.__len__())
#         fmt_str += '    Root Location: {}\n'.format(self.root)
#         tmp = '    Transforms (if any): '
#         fmt_str += '{0}{1}\n'.format(tmp, self.transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
#         tmp = '    Target Transforms (if any): '
#         fmt_str += '{0}{1}'.format(tmp, self.target_transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
#         return fmt_str

#     def _check_exists(self):
#         return os.path.exists(os.path.join(self.root, 'data', 'images')) and \
#             os.path.exists(self.classes_file)

#     def download(self):
#         """Download the FGVC-Aircraft data if it doesn't exist already."""
#         from six.moves import urllib
#         import tarfile

#         if self._check_exists():
#             return

#         # prepare to download data to PARENT_DIR/fgvc-aircraft-2013.tar.gz
#         print('Downloading %s ... (may take a few minutes)' % self.url)
#         parent_dir = os.path.abspath(os.path.join(self.root, os.pardir))
#         tar_name = self.url.rpartition('/')[-1]
#         tar_path = os.path.join(parent_dir, tar_name)
#         data = urllib.request.urlopen(self.url)

#         # download .tar.gz file
#         with open(tar_path, 'wb') as f:
#             f.write(data.read())

#         # extract .tar.gz to PARENT_DIR/fgvc-aircraft-2013b
#         data_folder = tar_path.strip('.tar.gz')
#         print('Extracting %s to %s ... (may take a few minutes)' % (tar_path, data_folder))
#         tar = tarfile.open(tar_path)
#         tar.extractall(parent_dir)

#         # if necessary, rename data folder to self.root
#         if not os.path.samefile(data_folder, self.root):
#             print('Renaming %s to %s ...' % (data_folder, self.root))
#             os.rename(data_folder, self.root)

#         # delete .tar.gz file
#         print('Deleting %s ...' % tar_path)
#         os.remove(tar_path)

#         print('Done!')


def npy_loader(path):
    img = np.load(path)

    if img.dtype == np.uint8:
        img = img.astype(np.float32)
        img = img/127.5 - 1.
    elif img.dtype == np.float32:
        img = img * 2 - 1.
    else:
        raise NotImplementedError

    img = torch.Tensor(img)
    if len(img.size()) == 4:
        img.squeeze_(0)

    return img
