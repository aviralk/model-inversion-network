import torch
from gan_training.metrics import inception_score
from gan_training.metrics.fid_score import calculate_fid_given_images
import numpy as np
from torch.autograd import Variable
import torch.distributions as td

class Evaluator(object):
    def __init__(self,
                 generator,
                 zdist,
                 ydist,
                 batch_size=64,
                 inception_nsamples=5000,
                 device=None,
                 fid_real_samples=None,
                 fid_sample_size=10000):
        self.generator = generator
        self.zdist = zdist
        self.ydist = ydist
        self.inception_nsamples = inception_nsamples
        # Only used in compute_inception_score
        self.batch_size = batch_size
        self.device = device
        # This has to be a np
        if fid_real_samples is not None:
            self.fid_real_samples = fid_real_samples.numpy()
            self.fid_sample_size = fid_sample_size

    def compute_inception_score(self):
        self.generator.eval()
        imgs = []
        while (len(imgs) < self.inception_nsamples):
            ztest = self.zdist.sample((self.batch_size, ))
            ytest = self.ydist.sample((self.batch_size, ))

            samples = self.generator(ztest, ytest)
            samples = [s.data.cpu().numpy() for s in samples]
            imgs.extend(samples)

        inception_imgs = imgs[:self.inception_nsamples]
        score, score_std = inception_score(
            inception_imgs,
            device=self.device,
            resize=True,
            splits=10,
            batch_size=self.batch_size)

        fid_imgs = np.array(imgs[:self.fid_sample_size])
        if self.fid_real_samples is not None:
            fid = calculate_fid_given_images(
                self.fid_real_samples,
                fid_imgs,
                batch_size=self.batch_size,
                cuda='cuda' in self.device.type)

        return score, score_std, fid

    def create_samples(self, z, y=None):
        self.generator.eval()
        batch_size = z.size(0)
        # Parse y
        if y is None:
            y = self.ydist.sample((batch_size, ))
        elif isinstance(y, int):
            y = torch.full(
                (batch_size, ), y, device=self.device, dtype=torch.int64)
        # Sample x
        with torch.no_grad():
            x = self.generator(z, y)
        return x

class ForwardModelEvaluator(object):
    def __init__(self, generator, forward_model, zdist, ydist, device, lamda, nu, num_itrs, z_dim, maximize=-1, batch_size=64):
        self.generator = generator
        self.forward_model = forward_model
        self.zdist = zdist
        self.ydist = ydist

        self.batch_size = batch_size
        self.device = device
        self.z_dim = z_dim

        self.num_itrs = num_itrs
        self.lamda = lamda
        self.nu = nu

        self.maximize = maximize
        self.std_normal = td.Normal(loc=0.0, scale=1.0)

    def create_samples(self, ystart=None, zstart=None):
        # import ipdb; ipdb.set_trace()
        self.generator.eval()
        self.forward_model.eval()
        
        # Create a batched version to fasten this up
        # ystart = ystart.unsqueeze(-1).repeat(self.batch_size, 1)
        ystart = ystart.repeat(self.batch_size)
        ystart = ystart + torch.rand_like(ystart, device=self.device) * 0.1
        if zstart is not None:
            zstart = zstart.unsqueeze(0).repeat(self.batch_size, 1)

        y = Variable(ystart, requires_grad=True)
        if zstart is not None:
            z = Variable(zstart, requires_grad=True)
        else:
            z = torch.rand(self.batch_size, self.z_dim, requires_grad=True, device=self.device)
        
        opt_y_z = torch.optim.Adam([y, z], lr=1e-4)
        set_images = []

        for idx in range(self.num_itrs):
            if idx % 100 == 1:
                print (idx)
                print ('Forward Model score: ', self.forward_model(sampled_x).mean())
                print ('Reconstruction Error: ', (self.forward_model(sampled_x) - y).pow(2).mean())
                print ('ZDist Log Prob: ', self.zdist.log_prob(z).sum(-1).mean())
            with torch.set_grad_enabled(True):
                sampled_x = self.generator(z, y)
                loss_t = self.maximize * self.forward_model(sampled_x).sum() +\
                                self.lamda * (self.forward_model(sampled_x) - y.detach()).pow(2).sum() -\
                                    self.nu * self.zdist.log_prob(z).sum()
            
            if idx % 100 == 1:
                set_images.append(sampled_x)
            opt_y_z.zero_grad()
            loss_t.backward() 
            opt_y_z.step()
        
        with torch.no_grad():
            final_samples = self.generator(z, y)
        
        # import ipdb; ipdb.set_trace()
        # print ('y_values: ', y.item()) 
        if self.maximize:
            y_max_idx = torch.argmax((-y).squeeze(-1))
        else:
            y_max_idx = torch.argmax(y.squeeze(-1))

        maximized_sample = final_samples[y_max_idx]
        return final_samples, maximized_sample, set_images
