# Model Inversion Networks: Code for Semantic Optimization over Face Images
This is the code for Model Inversion Networks, for the experiment on semantic optimization over face images. 

This code is built on top of code for the image generation experiments in [Variational Discriminator Bottleneck: Improving Imitation Learning, Inverse RL, and GANs by Constraining Information Flow](https://xbpeng.github.io/projects/VDB/index.html), which can be found at (https://github.com/akanazawa/vgan).

# Usage: This is the same as the VGAN repository
First download your data and put it into the `./data` folder. To get cleaned version of the IMDB-Wiki faces dataset, please follow the instructions in: (https://github.com/jingkuan/Age-Gender-Pred).  

To train a new model, first create a config script similar to the ones provided in the `./configs` folder.  You can then train you model using
```
python train.py PATH_TO_CONFIG
```

You can monitor the training with tensorboard:
```
tensorboard --logdir output/<MODEL_NAME>/monitoring/
```

# Experiments
To generate samples, use
```
python test.py PATH_TO_CONIFG
```

## Example run for Model Inversion networks
```
python train.py imdb.yaml --gpu=1 --eval_best_z 
```
the `eval_best_z` flag is used to turn on the inference procedure (Approx-Infer, Section 3.2) which finds the best y and z by running gradient based optimization on y and z. The code for this can be found in gan_training/eval.py.

the code for reweighting (Section 3.3) can be found in lines 75-91 and lines 182-192 in gan_training/weighted_train.py.

## Config File Hyperparameters

`nlabels: 20`: Number of bins used for computing weights (see section 3.5, Details)
`continuous: true`: Whether to pass the score as a continuous variable or discretize it (We choose continuous by default)
`cropped_above: false`: For experiment on face images, we remove the part of the dataset below a certain threshold of ages, this is used to control whether the part of the dataset above, or below a certain age is removed. By default set to false, meaning that part of the dataset below a threshold specified in cropping_score (below) is removed from training.
`cropping_score: 25`: Threshold of image scores in the dataset that need to be removed (15 or 25 for our experiments)

